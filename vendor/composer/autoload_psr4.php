<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'unreal4u\\TelegramAPI\\' => array($vendorDir . '/unreal4u/telegram-api/src'),
    'fXmlRpc\\' => array($vendorDir . '/lstrojny/fxmlrpc/src/fXmlRpc'),
    'anlutro\\cURL\\' => array($vendorDir . '/anlutro/curl/src'),
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'TrueBV\\' => array($vendorDir . '/true/punycode/src'),
    'TelegramBot\\Api\\' => array($vendorDir . '/telegram-bot/api/src'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Slim\\Views\\' => array($vendorDir . '/slim/php-view/src', $vendorDir . '/slim/twig-view/src'),
    'Slim\\Csrf\\' => array($vendorDir . '/slim/csrf/src'),
    'Slim\\' => array($vendorDir . '/slim/slim/Slim'),
    'Rych\\OTP\\' => array($vendorDir . '/rych/otp/src'),
    'React\\Promise\\' => array($vendorDir . '/react/promise/src'),
    'React\\' => array($vendorDir . '/react/react/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'ParagonIE\\ConstantTime\\' => array($vendorDir . '/paragonie/constant_time_encoding/src'),
    'Otp\\' => array($vendorDir . '/christian-riesen/otp/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Mdanter\\Ecc\\' => array($vendorDir . '/mdanter/ecc/src'),
    'Longman\\TelegramBot\\' => array($vendorDir . '/longman/telegram-bot/src'),
    'League\\Url\\' => array($vendorDir . '/league/url/src'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'Http\\Promise\\' => array($vendorDir . '/php-http/promise/src'),
    'Http\\Message\\' => array($vendorDir . '/php-http/message-factory/src'),
    'Http\\Discovery\\' => array($vendorDir . '/php-http/discovery/src'),
    'Http\\Client\\' => array($vendorDir . '/php-http/httplug/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'FG\\' => array($vendorDir . '/fgrosse/phpasn1/lib'),
    'Dnetix\\MasterPass\\' => array($vendorDir . '/dnetix/masterpass/src'),
    'CurrencyConverter\\' => array($vendorDir . '/ujjwal/currency-converter/src'),
    'Comodojo\\Xmlrpc\\' => array($vendorDir . '/comodojo/xmlrpc/src'),
    'Comodojo\\RpcClient\\' => array($vendorDir . '/comodojo/rpcclient/src'),
    'Comodojo\\Httprequest\\' => array($vendorDir . '/comodojo/httprequest/src'),
    'Comodojo\\Exception\\' => array($vendorDir . '/comodojo/exceptions/src/Exception'),
    'Coinbase\\Wallet\\' => array($vendorDir . '/coinbase/coinbase/src'),
    'Clickatell\\' => array($vendorDir . '/arcturial/clickatell/src', $vendorDir . '/arcturial/clickatell/test'),
    'BlockScore\\' => array($vendorDir . '/blockscore/blockscore-php/lib'),
    'BitWasp\\Stratum\\' => array($vendorDir . '/bitwasp/stratum/src'),
    'BitWasp\\Buffertools\\' => array($vendorDir . '/bitwasp/buffertools/src/Buffertools'),
    'BitWasp\\Bitcoin\\' => array($vendorDir . '/bitwasp/bitcoin/src'),
    'Base32\\' => array($vendorDir . '/christian-riesen/base32/src'),
);
