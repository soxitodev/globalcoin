<?php

// Run on console:
// php -f .\sample\introduction\FundAddressWithFaucetEndpoint.php

require __DIR__ . '/../bootstrap.php';

use BlockCypher\Auth\SimpleTokenCredential;
use BlockCypher\Client\FaucetClient;
use BlockCypher\Rest\ApiContext;

$apiContext = ApiContext::create(
    'test', 'bcy', 'v1',
    new SimpleTokenCredential('f6537aac966e4a458f77dd1c1991cc29'),
    array('mode' => 'sandbox', 'log.LogEnabled' => true, 'log.FileName' => 'BlockCypher.log', 'log.LogLevel' => 'DEBUG')
);

$faucetClient = new FaucetClient($apiContext);
$faucetResponse = $faucetClient->fundAddress('mxivitzbcQAdGjDDARSAXJtmjZ9pUS9M1P', 100000);

/// For Sample Purposes Only.
$faucet = new \BlockCypher\Api\Faucet();
$faucet->setAddress('mxivitzbcQAdGjDDARSAXJtmjZ9pUS9M1P');
$faucet->setAmount(100000);

ResultPrinter::printResult("Fund Bcy Test Address", "FaucetResponse", null, $faucet, $faucetResponse);