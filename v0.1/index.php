<?php
/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2016/10/20
 * Time: 9:26 AM
 */
/**
 * @SWG\Info(title="Layby Cafe API", version="1.1")
 */

/**
 * @SWG\Get(
 *     path="resource.json",
 *     @SWG\Response(response="200", description="Layby Cafe")
 * )
 */
//namespace MasterCard\Api\Moneysend;
session_start();
/*require("functions/classes/untor.php");
$tor = new untor();*/
include '../vendor/autoload.php';

require '../vendor/tixastronauta/acc-ip/src/TixAstronauta/AccIp/AccIp.php';
\BlockScore\BlockScore::setApiKey('sk_test_27a232ff381530e8bb132256163bd061');


use Clickatell\Rest;
use Clickatell\ClickatellException;

include 'functions/MainController.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$container['view'] = new \Slim\Views\PhpRenderer("../templates/");

$app = new \Slim\App;

//$curl = new anlutro\cURL\cURL;

spl_autoload_register(function ($classname) {
    require ("../classes/" . $classname . ".php");
});

$container = $app->getContainer();

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('templates', [
        'cache' => 'functions/classes/cache'
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

$container['csrf'] = function ($c) {
    return new \Slim\Csrf\Guard;
};
// Register middleware for all routes
// If you are implementing per-route checks you must not add this

//------------------------------System Functions----------------------//
// Creates New Profile POst /consumers
$app->post('/register', function (Request $request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new users;

    $login = $mapper->registerProfile($data);

    return $login;

});
// login//
$app->post('/login', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \users();

   $login = $mapper->login($data);


    return $login;


});

//Get Profile//
$app->get('/profile', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();
    $contractor = $data['contractor_no'];
    $mapper = new users();
    $getBank = $mapper->getProfile($contractor);

    return $getBank;


});

//upload image//
$app->post('/profile/image', function (Request $request,Response $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new users;

    $image = $mapper->imageUpload($data);

    return $image;


});

$app->post('/profile/update', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new users();

    $profileUpdate = $mapper->profileUpdate($data);

    return $profileUpdate;


});
//verify account//
$app->post('/verify', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new users;

    $verify = $mapper->verify($data);

    return $verify;


});
// Clef Login //
$app->post('/clef', function ($request, $response, $args) {

    $data = $request->getParsedBody();
    $login_data = [];
    $login_data['clef_id'] = filter_var($data['clef_id'], FILTER_SANITIZE_STRING);
    $login_data['email'] = filter_var($data['email'], FILTER_SANITIZE_STRING);

    $clef_id =$login_data['clef_id'];
    $email =$login_data['email'];

    $mapper = new users;

    $login = $mapper->clef($clef_id,$email);

    return $login;


});

//Add Bank Account//
$app->post('/bank', function ($request, $response, $args) {

    $data = $request->getParsedBody();
    $mapper = new users();
    $bank = $mapper->addBank($data);
    return $bank;


});
//Add Bank Account//
$app->get('/bank', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();
    $contractor = $data['contractor_no'];
    $mapper = new users();
    $getBank = $mapper->getBank($contractor);

    return $getBank;


});

//Identity Verification//
$app->get('/user/verification', function (request $request,Response $response, $args) {

    $data = $request->getQueryParams();

    $mapper = new users();

     $verifyID = $mapper->verifyID($data);

    return $verifyID;

});

//---------------------forgot password-----------------------------//
$app->get('/password/forgot', function (Request $request, Response $response, $args) {

    $data = $request->getQueryParams();

    $mapper = new password();

    $generateLink = $mapper->generateLink($data);

    return $generateLink;

});
$app->get('/password/verify/link', function (Request $request, Response $response, $args) {

    $data = $request->getQueryParams();

    $mapper = new password();

    $verifyLink = $mapper->verifyLink($data);

    return $verifyLink;

});

//Password change request//
$app->post('/password/change', function ($request, $response, $args) {


    $data = $request->getParsedBody();

    $mapper = new password();

    $changePass = $mapper->changePass($data);

    return $changePass;

});

//Verify Password//
$app->post('/password/verify', function ($request, $response, $args) {


    $data = $request->getParsedBody();


    $mapper = new password();

    $changePass = $mapper->verifyPass($data);

    return $changePass;

});
//----------------------------------BitcoinFunctions------------------------------//
//creates new GBC Address//
$app->post('/bitcoin/newaddress', function ($request, $response, $args) {

    $data = $request->getParsedBody();
    $mapper = new bitcoins();
    $bitcoinAddress = $mapper->createAddress($data);

    return $bitcoinAddress;


});
//Gets GBC balance/
$app->get('/bitcoin/balance', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();

    $label = @$data['contractor_no'];
    $mapper = new bitcoins();
    $bitcoinBalance = $mapper->getBalance($label);

    return $bitcoinBalance;


});

//Withdraw Bitcoins to Card//
$app->post('/bitcoin/withdraw', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new bloccypher();

    $withdraw = $mapper->withDraw($data);

    return $withdraw;

});
//sales//
$app->post('/bitcoin/sale', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new bitcoins();

    $submitSale = $mapper->submitSale($data);

    return $submitSale;

});
$app->get('/bitcoin/sale/from', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();

    $mapper = new bitcoins();

    $submitSale = $mapper->getSalesFrom($data);

    return $submitSale;

});
$app->get('/bitcoin/sale/to', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();

    $mapper = new bitcoins();

    $submitSale = $mapper->getSales($data);

    return $submitSale;

});
//sell coins//
$app->post('/bitcoin/transfare', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new bitcoins();

    $sellCoins = $mapper->transfareCoins($data);

    return $sellCoins;

});
//create trade//
$app->post('/bitcoin/trade', function (Request $request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new bitcoins();

    $createTrade = $mapper->createTrade($data);

    return $createTrade;

});
// Get Sellers//
$app->get('/bitcoin/sellers', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();

    $mapper = new bitcoins();

    $bitcoinBalance = $mapper->getSellers($data);

    return $bitcoinBalance;


});


//--------------BlockCypher-------------------------//
//creates new GBC Address//
$app->post('/bitcoin/new_address', function ($request, $response, $args) {

    $data = $request->getParsedBody();
    $mapper = new bloccypher();
    $bitcoinAddress = $mapper->address($data);

    return $bitcoinAddress;


});
$app->get('/bitcoin/address_balance', function (Request $request, Response $response, $args) {

    $data = $request->getQueryParams();
    $label = $data['contractor_no'];
    $mapper = new bloccypher();
    $bitcoinAddress = $mapper->balance($label);

    return $bitcoinAddress;


});
$app->get('/bitcoin/transactions', function (Request $request, Response $response, $args) {

    $data = $request->getQueryParams();
    $label = $data['contractor_no'];
    $mapper = new bitcoins();
    $bitcoinAddress = $mapper->transactions($label);

    return $bitcoinAddress;


});
$app->post('/bitcoin/create_transaction', function (Request $request, Response $response, $args) {

    $data = $request->getParsedBody();
    $mapper = new bloccypher();
    $bitcoinAddress = $mapper->createTransaction($data);

    return $bitcoinAddress;


});
//------------------------ethereum---------------------------------//

$app->get('/ethereum/address', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();

    $label = $data['contractor_no'];
    $mapper = new ethereum();
    $bitcoinBalance = $mapper->address($label);

    return $bitcoinBalance;


});

//--------------------------------Card Function--------------------------------//
$app->get('/cards', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();
    $cardNo = $data['contractor_no'];
    $mapper = new cards();
    $cardBalance = $mapper->getCard($cardNo);

    return $cardBalance;


});
$app->get('/cards/statement', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();
    $contractor_no = $data['contractor_no'];
    $mapper = new cards();
    $cardBalance = $mapper->statement($contractor_no);

    return $cardBalance;


});
//Link Card to profile then allocate it to the user//
$app->post('/allocate/cards', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \cards();

    $allocateCard = $mapper->allocateCard($data);

    return $allocateCard;

});

//Load a card with the requested amount and deduct the amount off the profile.//
$app->post('/load/cards/funds', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \cards();

    $allocateCard = $mapper->loadCard($data);

    return $allocateCard;

});

//Deduct requested amount from card and load the amount back to the profile.//
$app->post('/reverse/cards/funds', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \cards();

    $allocateCard = $mapper->reverseFundsToprofile($data);

    return $allocateCard;

});
//Deduct requested amount from card as a fee.//
$app->post('/cards/fees', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \cards();

    $fees = $mapper->fees($data);

    return $fees;

});

//-----------------------------------master card functions---------------------//
$app->post('/mastercard/moneysend', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \masterCard();

    $allocateCard = $mapper->moneySend($data);

    return $allocateCard;

});

//---------------------------------visa-------------------------------------//

$app->post('/visadirect/fundstransfer', function ($request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \visa();

    $fundstransfer = $mapper->funds_transafre($data);

    return $fundstransfer;

});

//--------------------Fica Users-----------------------------------//

$app->post('/fica/enroll', function (Request $request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \fica();

    $fica = $mapper->enroll($data);

    return $fica;

});
$app->get('/fica/documents', function (Request $request,Response $response, $args) {

    $data = $request->getQueryParams();

    $mapper = new \fica();

    $fica = $mapper->getDocument($data);

    return $fica;

});
$app->post('/fica/recognize', function (Request $request, $response, $args) {

    $data = $request->getParsedBody();

    $mapper = new \fica();

    $fundstransfer = $mapper->recognize($data);

    return $fundstransfer;

});

$app->run();