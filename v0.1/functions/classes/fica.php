<?php

/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/07/13
 * Time: 9:47 AM
 */
class fica
{
public static function getDocument($data){

    include 'config/dbconnect.php';

    $colls = ['file','gallery_name','subject_id'];
    $db->where ("contractor_no",$data['contractor_no']);
    $document = $db->getOne('documents',$colls);

    return json_encode($document);

}

    public static function enroll($data){

        include 'config/dbconnect.php';
        include 'config/kairos.php';

        $db->where ("contractor_no",$data['file_data']['contractor_no']);
        $db->getOne('documents');

        if($db->count>0){
            $response =[
                'code'=>'1000',
                'message'=>'File Already uploaded please verify your identification'
            ];
        }else{

        $result = $api->post("/enroll",json_encode($data['fica_data']));

       $idendification = json_decode($result->response);

        $face_id = $idendification->face_id;

       if($face_id!=null){
           $documentsData=[
               'contractor_no' => $data['file_data']['contractor_no'],
               'face_id' => $face_id,
               'face_recognition' => 'enroll',
               'file' => $data['file_data']['file'],
               'name' => $data['file_data']['name'],
               'old_name' => $data['file_data']['old_name'],
               'replaced' => $data['file_data']['replaced'],
               'size' => $data['file_data']['size'],
               'size2' => $data['file_data']['size2'],
               'type' => $data['file_data']['type'],
               'doc_type' => $data['file_data']['doc_type'],
               'extension' => $data['file_data']['extension'],
               'status' => $idendification->images[0]->transaction->status,
               'gallery_name' => $idendification->images[0]->transaction->gallery_name,
               'subject_id' => $idendification->images[0]->transaction->subject_id,
               'date' => $data['file_data']['date'],
           ];
            $db->insert('documents',$documentsData);
           if($db->count>0){

           $response =[

               'code'=>'2000',
               'message'=>'Idendity Document uploaded please verify your identification'
           ];
        }
       }

        }

    return json_encode($response);

    }

    public static function recognize($data){

        include 'config/kairos.php';

        $result = $api->post("/recognize",json_encode($data));

        $recognize = json_decode($result->response);

        if($recognize->Errors[0]->ErrCode=='5001'){
            $response =[
                'code'=>$recognize->Errors[0]->ErrCode,
                'message'=>$recognize->Errors[0]->Message,
            ];
        }
        elseif($recognize->images[0]->transaction->status=='failure'){
            $response =[
                'code'=>'5002',
                'message'=>'Failed to verify identity, because '.$recognize->images[0]->transaction->message,
            ];
        }
        elseif($recognize->images[0]->transaction->status=='success'){

            $response =[
                'code'=>'5000',
                'message'=>'Identification Verified',
            ];
        }
        elseif($recognize->Errors[0]->ErrCode=='5002'){

              $response =[
                'code'=>$recognize->Errors[0]->ErrCode,
                'message'=>$recognize->Errors[0]->Message,
            ];
        }


        return json_encode($response);
    }

}