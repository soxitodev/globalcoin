<?php

if(@$emailFormat=='changePassword'){
    $response ='
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0;padding:0;height:100%!important;width:100%!important">
    <tbody><tr>
        <td valign="top" align="center">
            <table width="600" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #dddddd;background-color:white">
                <tbody><tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color:#ffffff;border-bottom:0">
                            <tbody><tr>
                                <td>

                                    <img width="140" align="left" style="border:none;padding:30px;min-height:auto;line-height:100%;outline:none;text-decoration:none" src="http://app.globalcoinlife.com/img/logo.png" class="CToWUd">

                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="10" border="0">
                            <tbody><tr>
                                <td valign="top" style="background-color:white">

                                    <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody><tr>
                                            <td valign="top" style="padding-top:0">
                                                <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left">
                                                    <p style="margin-top:0">Hi '.@$user['first_name'].'</p>
                                                <p>A change password request has been generated for your account. To change your password, please click on the link below.</p>
                                                <ul>
                                                </ul>
                                                <center>
                                               <a href="https://app.globalcoinlife.com/changepass?code='.$emailCode.'&email='.$user['email'].'" style="background-color:#ffc400;border:0px solid #355898;padding:10px;text-align:center;border-radius:5px;max-width:100px;color:#fff;font-weight:bold;text-decoration:none;display:block">Confirm Code</a>
                                                   <p>This link expires in 24 Hours</p>
                                                   </center>
                                                    <p>Regards<br>Global Coin Life Team</p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
            <table width="600" cellspacing="0" cellpadding="10" border="0" style="margin-top:30px;border-top:0">
                            <tbody><tr>
                                <td valign="top" align="center">
                                 <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody><tr>
                                            <td valign="middle" style="border-top:1px solid #f5f5f5" colspan="2">
                                                <div style="color:#959595;font-family:Arial;font-size:11px;line-height:125%;text-align:center">


                                                    You received this email because an account was registered for Global Coin using the address '.@$user['email'].'.


                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>


                                </td>
                            </tr>
                            </tbody></table>
                            </tbody></table>
            <br>
        </td>
    </tr>
    </tbody></table>';

}

if(@$emailFormat=='signUp'){
    $response ='
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0;padding:0;height:100%!important;width:100%!important">
    <tbody><tr>
        <td valign="top" align="center">
            <table width="600" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #dddddd;background-color:white">
                <tbody><tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color:#ffffff;border-bottom:0">
                            <tbody><tr>
                                <td>

                                    <img width="140" align="left" style="border:none;padding:30px;min-height:auto;line-height:100%;outline:none;text-decoration:none" src="http://app.globalcoinlife.com/img/logo.png" class="CToWUd">

                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="10" border="0">
                            <tbody><tr>
                                <td valign="top" style="background-color:white">

                                    <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody><tr>
                                            <td valign="top" style="padding-top:0">
                                                <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left">
                                                    <p style="margin-top:0">Hi '.@$register_data['first_name'].'</p>
                                                <p>Hey '.@$register_data['first_name'].' Welcome to In Coin Your verification code is '.@$verCode.'.</p>
                                                <p>Please Click the link bellow to verify your account using the given code</p>
                                                <ul>
                                                </ul>
                                                <center>
                                               <a href="https://app.incoin.co.za/verify" style="background-color:#ffc400;border:0px solid #355898;padding:10px;text-align:center;border-radius:5px;max-width:100px;color:#fff;font-weight:bold;text-decoration:none;display:block">Confirm Code</a>
                                                   <p>This link expires in 24 Hours</p>
                                                   </center>
                                                    <p>Regards<br>Global Coin Life Team</p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
            <table width="600" cellspacing="0" cellpadding="10" border="0" style="margin-top:30px;border-top:0">
                            <tbody><tr>
                                <td valign="top" align="center">
                                 <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody><tr>
                                            <td valign="middle" style="border-top:1px solid #f5f5f5" colspan="2">
                                                <div style="color:#959595;font-family:Arial;font-size:11px;line-height:125%;text-align:center">


                                                    You received this email because an account was registered for Global Coin using the address '.@$user['email'].'.


                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>


                                </td>
                            </tr>
                            </tbody></table>
                            </tbody></table>
            <br>
        </td>
    </tr>
    </tbody></table>';

}

if(@$emailFormat=='invoiceSeller'){
    $response ='
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0;padding:0;height:100%!important;width:100%!important">
    <tbody><tr>
        <td valign="top" align="center">
            <table width="600" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #dddddd;background-color:white">
                <tbody><tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color:#ffffff;border-bottom:0">
                            <tbody><tr>
                                <td>

                                    <img width="140" align="left" style="border:none;padding:30px;min-height:auto;line-height:100%;outline:none;text-decoration:none" src="http://app.incoin.co.za/assets/images/logo-blue.png" class="CToWUd">

                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="10" border="0">
                            <tbody><tr>
                                <td valign="top" style="background-color:white">

                                    <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody><tr>
                                            <td valign="top" style="padding-top:0">
                                                <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left">
                                                    <p style="margin-top:0">Hi '.@$user['first_name'].'</p>
                                                <p>You have you have a Bitcoin transaction that has been created.</p>
                                                <p>Transaction Details :</p>
                                                <ul>
                                                <li>Ref : '.@$sale_data['ref'].'</li>
                                                <li>BTC : '.@$sale_data['coins'].'</li>
                                                <li>Zar : '.@$sale_data['amount'].'</li>
                                                </ul>
                                                <h5>Note: always confirm paid Invoice/Ref numbers as coins are not refundable.</h5>
                                            
                                                    <p>Regards<br>Global Coin Life Team</p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
            <table width="600" cellspacing="0" cellpadding="10" border="0" style="margin-top:30px;border-top:0">
                            <tbody><tr>
                                <td valign="top" align="center">
                                 <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody><tr>
                                            <td valign="middle" style="border-top:1px solid #f5f5f5" colspan="2">
                                                <div style="color:#959595;font-family:Arial;font-size:11px;line-height:125%;text-align:center">


                                                    You received this email because an account was registered for Global Coin using the address '.@$user['email'].'.


                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>


                                </td>
                            </tr>
                            </tbody></table>
                            </tbody></table>
            <br>
        </td>
    </tr>
    </tbody></table>';

}

if(@$emailFormat=='paymentSent'){
    $response ='
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0;padding:0;height:100%!important;width:100%!important">
    <tbody><tr>
        <td valign="top" align="center">
            <table width="600" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #dddddd;background-color:white">
                <tbody><tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="0" border="0" style="background-color:#ffffff;border-bottom:0">
                            <tbody><tr>
                                <td>

                                    <img width="140" align="left" style="border:none;padding:30px;min-height:auto;line-height:100%;outline:none;text-decoration:none" src="http://app.globalcoinlife.com/img/logo.png" class="CToWUd">

                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center">

                        <table width="600" cellspacing="0" cellpadding="10" border="0">
                            <tbody><tr>
                                <td valign="top" style="background-color:white">
                                    <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody>
                                        <tr>
                                            <td valign="top" style="padding-top:0">
                                                <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left">
                                                <p style="margin-top:0">Hi '.@$user['first_name'].'</p>
                                                <p>Your Bitcoin Investment has been confirmed please visit Incoin to view your coins.</p>
                                                <p>Transaction Details :</p>
                                                <ul>
                                                <li>Ref : '.@$transaction_data['ref'].'</li>
                                                <li>BTC : '.@$transaction_data['requestAmount'].'</li>
                                             </ul>
                                             <p>Regards<br>Incoin Team</p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
            <table width="600" cellspacing="0" cellpadding="10" border="0" style="margin-top:30px;border-top:0">
                            <tbody><tr>
                                <td valign="top" align="center">
                                 <table width="100%" cellspacing="0" cellpadding="10" border="0">
                                        <tbody><tr>
                                            <td valign="middle" style="border-top:1px solid #f5f5f5" colspan="2">
                                                <div style="color:#959595;font-family:Arial;font-size:11px;line-height:125%;text-align:center">


                                                    You received this email because an account was registered for Incoin using the address '.@$user['email'].'.


                                                </div>
                                            </td>
                                        </tr>
                                        </tbody></table>


                                </td>
                            </tr>
                            </tbody></table>
                            </tbody></table>
            <br>
        </td>
    </tr>
    </tbody></table>';

}