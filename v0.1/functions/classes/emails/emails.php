<?php
/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2016/11/07
 * Time: 2:12 PM
 */
 //require '../../vendor/autoload.php';

include 'email_templates.php';

$mail = new PHPMailer;
$deploy ='local';
if(@$deploy=='local'){

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.incoin.co.za';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'noreply@incoin.co.za';                 // SMTP username
    $mail->Password = 'qWk6iUpRw*2U';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                   // TCP port to connect to

}
else{

    $mail->Host = 'mail.incoin.co.za';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'noreply@incoin.co.za';                 // SMTP username
    $mail->Password = 'qWk6iUpRw*2U';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;
}

$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->isHTML(true);


if(@$emailFormat=='changePassword') {

    $mail->setFrom('noreply@incoin.co.za', 'In Coin');
    $mail->addAddress(@$user['email'], @$user['first_name']);     // Add a recipient
    $mail->Subject = 'Change Password';
    $mail->Body = $response;
    $mail->AltBody = 'Your Item has been added successfully';
    $mail->send();
}

if(@$emailFormat=='invoiceSeller') {
    $mail->setFrom('noreply@incoin.co.za', 'In Coin');
    $mail->addAddress(@$user['email'], @$user['first_name']);
    $mail->Subject = 'Invoice';
    $mail->Body = $response;
    $mail->AltBody = 'You have a pending payment on In Coin';
}

if(@$emailFormat=='signUp') {
    $mail->setFrom('noreply@incoin.co.za', 'In Coin');
    $mail->addAddress(@$user['email'], @$user['first_name']);
    $mail->Subject = 'Verify Account';
    $mail->Body = $response;
    $mail->AltBody = 'Verify Account';
}

if(@$emailFormat=='paymentSent') {
    $mail->setFrom('noreply@incoin.co.za', 'In Coin');
    $mail->addAddress(@$user['email'], @$user['first_name']);
    $mail->Subject = 'Investment Processed';
    $mail->Body = $response;
    $mail->AltBody = 'You have a pending payment on In Coin';
}