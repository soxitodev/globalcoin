<?php

/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/06/21
 * Time: 11:24 AM
 */
class users
{
    //Register Account//
    public function registerProfile($data){

        $verCode = rand(10000,99999);
        $code = md5($verCode);
        $register_data = [];
        $userReg = $register_data['userName'] = filter_var($data['userName'], FILTER_SANITIZE_STRING);
        $register_data['first_name'] = filter_var($data['first_name'], FILTER_SANITIZE_STRING);
        $register_data['last_name'] = filter_var($data['last_name'], FILTER_SANITIZE_STRING);
        $register_data['email'] = filter_var($data['email'], FILTER_SANITIZE_STRING);
        $register_data['phone'] = filter_var($data['phone'], FILTER_SANITIZE_STRING);
        $register_data['password'] = filter_var($data['password'], FILTER_SANITIZE_STRING);
        require 'config/dbconnect.php';

        $db->where ("userName", $userReg);
        $db->orWhere ("phone", $register_data['phone']);
        $db->orWhere ("email", $register_data['email']);
        $db->getOne ("tbl_users");

        //validates info if exists//

        if ($db->count>0){

            $error =array(
                "code"=>"210",

                "message"=>"user/user details Already Exist please try again");

            return (json_encode($error,JSON_PRETTY_PRINT,True));


        }
        else {

            require 'config/keygen.php';
            // Password 3D encryption//
            $salt = getSalt();
            $salt = base64_encode($salt);
            $salt = str_replace('+', '.', $salt);

            $password = generateStrongPassword();

            $password3d = crypt($register_data['password'], '$2y$10$' . $salt . '$');

            if (filter_var($userReg, FILTER_VALIDATE_EMAIL)) {

                $data = Array(
                    'contractor_no' => $consumer_no,
                    'userName' => $register_data['userName'],
                    'first_name' => $register_data['first_name'],
                    'last_name' => $register_data['last_name'],
                    'email' => $register_data['email'],
                    'phone' => $register_data['phone'],
                    'tokenCode' => $code,
                    'mobileToken' => $verCode,
                    'profile_type' => 'email',
                    'active' => 'N',
                    'salt' => $salt,
                    'userPass' => $password3d,
                    'createdAt' => $db->now(),
                );

                $db->insert('tbl_users', $data);

                if($db->count>0){

                    $emailFormat = 'signUp';

                    $db->Where ("contractor_no", $consumer_no);
                    $db->Where ("email", $register_data['email']);
                    $user = $db->getOne ("tbl_users");

                    include 'emails/emails.php';
                    $emailUser = $mail->send();
                    if(!$mail->send()){
                        $response = array(
                            'code'=>328,
                            'message'=>'Mail error',
                            'email'=>$user['email'],
                            'mail'=>$mail->ErrorInfo
                        );
                    }else{

                        $response = array(
                            "code" => "209",
                            "message" => "User created successfully");
                    }

                    return json_encode($response, JSON_PRETTY_PRINT, True);

                }
            }
            else {

                $data = Array(
                    'contractor_no' => $consumer_no,
                    'userName' => $register_data['userName'],
                    'first_name' => $register_data['first_name'],
                    'last_name' => $register_data['last_name'],
                    'email' => $register_data['email'],
                    'phone' => $register_data['phone'],
                    'tokenCode' => $code,
                    'mobileToken' => $verCode,
                    'profile_type' => 'mobile',
                    'active' => 'N',
                    'salt' => $salt,
                    'userPass' => $password3d,
                    'createdAt' => $db->now(),
                );

                $db->insert('tbl_users', $data);

                //send sms//

                if($db->count>0){

                    $noticeType = "userRegistration";
                    include 'config/sms_functions.php';

                    $response = array(
                        "code" => "209",
                        "consumer_id" => $consumer_no,
                        "message" => "User created successfully");

                    return json_encode($response, JSON_PRETTY_PRINT, True);

                }


            }


        }


    }

    public function login($data){

        require 'config/dbconnect.php';
        $login_data = [];
        $login_data['userName'] = filter_var($data['userName'], FILTER_SANITIZE_STRING);
        $login_data['password'] = filter_var($data['password'], FILTER_SANITIZE_STRING);

        $db->orWhere("userName", $login_data['userName']);
        $consumer_data = $db->getOne("tbl_users");

        $salt = $consumer_data['salt'];
        $password = $consumer_data['userPass'];
        $active = $consumer_data['active'];
        $userID = $consumer_data['contractor_no'];

        if($db->count>0){

            if($active=='Y'){
                if($password==crypt($login_data['password'], '$2y$10$'.$salt.'$'))
                {


                    $userData =array(
                        'code'=>'602',
                        "user_id"=>$userID);

                    $lastLogin = Array (

                        'lastLogin' =>  $db->now()
                    );

                    $db->where("contractor_no", $userID);

                    $db->update ("tbl_users",$lastLogin);

                    return json_encode($userData,JSON_PRETTY_PRINT,True);
                }
                else
                {
                    $error =array(
                        "code"=>"500",
                        "message"=>"Wrong Password");

                    return json_encode($error,JSON_PRETTY_PRINT,True);
                }

            }
            else
            {

                $error =array(
                    "code"=>"600",
                    "user_id"=>$userID,
                    "message"=>"Activate Your Account");

                return json_encode($error,JSON_PRETTY_PRINT,True);

            }
        }

        else
        {

            $error =array(
                "code"=>'601',
                "message"=>'User '.$login_data['userName'].' Not Found Please Register');

            return json_encode($error,JSON_PRETTY_PRINT,True);

        }

    }

    public function verify($data){

        require 'config/dbconnect.php';
        $verify_data = [];
        $verify_data['userName'] = filter_var($data['userName'], FILTER_SANITIZE_STRING);
        $verify_data['token'] = filter_var($data['token'], FILTER_SANITIZE_STRING);


        $db->where("userName", $verify_data['userName']);
        $db->where("active", 'N');
        $user = $db->getOne ("tbl_users");

        if($db->count>0){

            $db->where("userName", $verify_data['userName']);
            $db->where("mobileToken",  $verify_data['token']);
            $db->where("active", 'N');
            $db->getOne ("tbl_users");
            //-------------------------------------email verification-----------------------------------//
            if($db->count>0){
            $data = array(
                'active'=>'Y'
            );
            $db->where("userName", $verify_data['userName']);
            $db->where("mobileToken",  $verify_data['token']);
            $db->where("active", 'N');
            $db->update ("tbl_users",$data);
            if($db->count>0){

                $response = array(
                    'code'=>209,
                    'userID'=>$user['contractor_no'],
                    'message'=>'Account verified'
                );
            }}
            else{
                $response = array(
                    'code'=>210,
                    'message'=>'Invalid Token'
                );
            }
        }

        else{

            $db->where("userName", $verify_data['userName']);
            $db->where("active", 'Y');
            $db->getOne ("tbl_users");

            if($db->count>0){
                $response = array(
                    'code'=>210,
                    'message'=>'Account already verified'
                );
            }else{
                $response = array(
                    'code'=>210,
                    'message'=>'Account Not Found Please Register'
                );
            }

        }

        return json_encode($response,JSON_PRETTY_PRINT, true);
    }

    public function clef($clef_id,$emailClef){

        require 'config/dbconnect.php';

        $db->where('email', $emailClef);
        $columns = array('merchant_id','role','email','active_account','verified_account','clef_id');
        $consumer_data = $db->getOne("merchant",$columns);

        $clefId = $consumer_data['clef_id'];
        $Active = $consumer_data['active_account'];
        $email = $consumer_data['email'];
        $consumer_id = $consumer_data['merchant_id'];
        $role = $consumer_data['role'];
        if($email==$emailClef) {
            if ($Active == 'Y') {

                if ($clef_id == $clefId) {

                    $userData = array(

                        "merchant_id" => $consumer_id,

                        "role" => $role);


                    $lastLogin = Array(

                        'lastLogin' => $db->now()
                        // active = !active;
                    );

                    $db->where("merchant_id", $consumer_id);

                    $db->update("merchant", $lastLogin);

                    return json_encode($userData, JSON_PRETTY_PRINT, True);
                } else {
                    require 'config/dbconnect.php';

                    $ClefAuth = Array(

                        'clef_id' => $clef_id
                        // active = !active;
                    );

                    $db->where("email", $email);
                    $db->update("merchant", $ClefAuth);

                    if ($db->count > 0) {

                        $error = array(
                            "code" => "512",

                            "message" => "Account Linked successfully now you can login with clef");

                        print_r(json_encode($error, JSON_PRETTY_PRINT, True));
                    } else {

                        $error = array(
                            "code" => "500",

                            "message" => "account already updated");

                        print_r(json_encode($error, JSON_PRETTY_PRINT, True));
                    }

                }

            }
        }else{
            $error = array(
                "code" => "500",

                "message" => "No Account Linked to email $emailClef. Please make sure you linked the email registed with the platform");

            print_r(json_encode($error, JSON_PRETTY_PRINT, True));

        }

    }

    public function getProfile($contractor){

        require 'config/dbconnect.php';

        $db->where("contractor_no", $contractor);
        $colls = array('userName','first_name','last_name','email','phone','can_sell','lastLogin');
        $contractor_data = $db->getOne("tbl_users",$colls);

        $db->where("contractor_no", $contractor);
        $db->orderBy("id","desc");
        $colls = array('file');
        $profileImage = $db->getOne("profile_image");

        //Gets Address//
        $colls = array('address1','address2','city','country','countrySubdivision','postalCode');
        $db->where("contractor_no", $contractor);
        $contractor_address = $db->getOne("contractor_address",$colls);
        if($contractor_data!=Null){

            $address =array(

                'address1'=>$contractor_address['address1'],
                'address2'=>$contractor_address['address2'],
                'city'=>$contractor_address['city'],
                'country'=>$contractor_address['country'],
                'countrySubdivision'=>$contractor_address['countrySubdivision'],
                'postalCode'=>$contractor_address['postalCode']
            );
            $profileData = array(
                'userName'=>$contractor_data['userName'],
                'first_name'=>$contractor_data['first_name'],
                'last_name'=>$contractor_data['last_name'],
                'email'=>$contractor_data['email'],
                'phone'=>$contractor_data['phone'],
                'can_sell'=>$contractor_data['can_sell'],
                'profile_image'=>$profileImage['file'],
                'lastLogin'=>$contractor_data['lastLogin'],
                'address'=>$address,
            );

            return json_encode($profileData,JSON_PRETTY_PRINT);
        }else{
            $error =array(
                "code"=>"501",

                "message"=>"No Profile Found");

            return json_encode($error,JSON_PRETTY_PRINT,True);
        }

    }

    public function getWallet($consumer_id=Null){

        require 'config/dbconnect.php';


        $db->where("consumer_id", $consumer_id);
        $columns = array('consumer_id','amount','useWallet');
        $consumer_data = $db->getOne("consumer_wallet",$columns);

        if($consumer_data!=Null){
            return json_encode($consumer_data);
        }else{

            $data = Array (
                "consumer_id" => $consumer_id,
                "amount" => 0,
                "useWallet" => '0'
            );
            $id = $db->insert ('consumer_wallet', $data);

            $error =array(
                "code"=>"501",

                "message"=>"Wallet added");

            print_r(json_encode($error,JSON_PRETTY_PRINT,True));
        }


    }

    public function getWalletLog($consumer_id=Null){

        require 'config/dbconnect.php';

        $db->where("wallet_id", $consumer_id);
        $db->orderBy("id","Desc");
        $cols = Array ("transaction_type", "log_info", "amount","balance", "createdAt");

        $walletLog = $db->get ("wallet_log", 15, $cols);

        return json_encode($walletLog);

    }

    public function updateWalletState($data){

        require 'config/dbconnect.php';

        $db->where('consumer_id', $data['wallet_id']);

        $use = $data['useWallet'];
        $data = Array (
            'useWallet' => $use
        );


        if ($db->update ('consumer_wallet', $data)){

            $data = array("message" => "updated");

            return  json_encode($data,JSON_PRETTY_PRINT,True);
        }else{

            $error =array(
                "code"=>"502",

                "message"=>"Sorry cant update layby");

            print_r(json_encode($error,JSON_PRETTY_PRINT,True));
        }

    }

    public function addBank($data){

        require 'config/dbconnect.php';

        $bank_data = [];
        $bank_data['contractor_no'] = filter_var(@$data['contractor_no'], FILTER_SANITIZE_STRING);

        $db->where ('contractor_no', $bank_data['contractor_no']);
        $db->getOne ("bank_account");

        if($db->count>0){
            $response = array(
                'code'=>627,
                "message" => 'Bank Account Exists');

            return json_encode($response, JSON_PRETTY_PRINT, true);

        }else{

            $db->insert ("bank_account",$data);

            if($db->count>0){
                $response = array(
                    'code'=>626,
                    "message" => 'Bank Account Added Successfully');

                return json_encode($response, JSON_PRETTY_PRINT, true);
            }
        }




    }

    public function getBank($contractor){

        require 'config/dbconnect.php';

        $db->where ('contractor_no', $contractor);

        $bank_account = $db->getOne ("bank_account");

        if($db->count>0){
            $bank = array('code'=>625,
                'bank_data'=>$bank_account
            );
            return json_encode($bank, JSON_PRETTY_PRINT);
        }else{
            $response = array('code'=>624,
                'message'=>'Please add Bank Account'
            );
            return json_encode($response, JSON_PRETTY_PRINT);
        }
    }

    public function verifyID($data){

        $person = \BlockScore\Person::create(array(
            'name_first' => 'John',
            'name_middle' => 'Pearce',
            'name_last' => 'Doe',
            'birth_day' => 23,
            'birth_month' => 8,
            'birth_year' => 1993,
            'document_type' => 'ssn',
            'document_value' => '0000',
            'address_street1' => '1 Infinite Loop',
            'address_street2' => 'Apt 6',
            'address_city' => 'Cupertino',
            'address_subdivision' => 'CA',
            'address_postal_code' => '95014',
            'address_country_code' => 'US',
            'note' => 'ref-id:12345'
        ));
        print_r($person);

    }

    public function imageUpload($data){

        require 'config/dbconnect.php';


        $file_data = [];

        $file_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);
        $file = $file_data['file'] = filter_var($data['file'], FILTER_SANITIZE_STRING);
        $name = $file_data['name'] = filter_var($data['name'], FILTER_SANITIZE_STRING);
        $old_name = $file_data['old_name'] = filter_var($data['old_name'], FILTER_SANITIZE_STRING);
        $replaced = $file_data['replaced'] = filter_var($data['replaced'], FILTER_SANITIZE_STRING);
        $size = $file_data['size'] = filter_var($data['size'], FILTER_SANITIZE_STRING);
        $size2 = $file_data['size2'] = filter_var($data['size2'], FILTER_SANITIZE_STRING);
        $extension = $file_data['extension'] = filter_var($data['extension'], FILTER_SANITIZE_STRING);
        $type = $file_data['type'] = filter_var($data['type'], FILTER_SANITIZE_STRING);

        $data = Array(
            'contractor_no' => $file_data['contractor_no'],
            'file' => $file,
            'name' => $name,
            'old_name' =>$old_name,
            'replaced' => $replaced,
            'size' => $size,
            'size2' => $size2,
            'type' => $type,
            'extension' => $extension

        );

        $db->Where("name", $name);
        $db->Where("contractor_no", $file_data['contractor_no']);

        $file = $db->getOne ("profile_image");

        if($db->count>0){

            $db->where ('contractor_no', $file_data['contractor_no']);
            $db->where ('name', $name);

            $db->update ('profile_image', $data);

            if($db->count>0){

                $response = array(
                    "code"=>"10",
                    "message" => "File uploaded Successfully");



            }

        }else{

            $db->insert('profile_image', $data);


            if($db->count>0){

                $response = array(
                    "code"=>"10",
                    "message" => "File uploaded Successfully");
            }
        }

        return  json_encode(@$response,JSON_PRETTY_PRINT,True);



    }

    public function profileUpdate($data){

        require 'config/dbconnect.php';


        if(@$data['contractor_no']!=null and $data['address1']!=null){
            $address_data = [];
            $address_data = [];
            //Address data
            $address_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);
            $address_data['address1'] = filter_var($data['address1'], FILTER_SANITIZE_STRING);
            $address_data['address2'] = filter_var($data['address2'], FILTER_SANITIZE_STRING);
            $address_data['city'] = filter_var($data['city'], FILTER_SANITIZE_STRING);
            $address_data['country'] = filter_var($data['country'], FILTER_SANITIZE_STRING);
            $address_data['countrySubdivision'] = filter_var($data['countrySubdivision'], FILTER_SANITIZE_STRING);
            $address_data['postalCode'] = filter_var($data['postalCode'], FILTER_SANITIZE_STRING);

            $address_dataArray = [
                'contractor_no'=> $address_data['contractor_no'],
                'address1'=> $address_data['address1'],
                'address2'=> $address_data['address2'],
                'city'=>$address_data['city'],
                'country'=>$address_data['country'],
                'countrySubdivision'=>$address_data['countrySubdivision'],
                'postalCode'=>$address_data['postalCode'],
            ];

            $db->Where("contractor_no", $address_data['contractor_no']);

            $db->getOne("contractor_address");

            if($db->count>0){

                $db->where ('contractor_no', $address_data['contractor_no']);
                $db->update ('contractor_address', $address_dataArray);

                if($db->count>0){

                    $response[] = array(
                        "code"=>"10",
                        "message" => "Address updated Successfully");

                }else{

                    $response[] = array(
                        "code"=>"12",
                        "message" => "Failed to update address");
                }

            }
            else{

                $db->insert('contractor_address', $address_dataArray);

                if($db->count>0){

                    $response[] = array(
                        "code"=>"11",
                        "message" => "Address added Successfully");
                }
            }

        }else{
            $response = array(
                "code"=>"12",
                "message" => "Failed to update address");
        }
        return json_encode($response);
    }


}