<?php

/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/06/26
 * Time: 11:40 AM
 */
class password
{

    public function generateLink($data){

        require 'config/dbconnect.php';

        $pass_data = [];

        $pass_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);
        $pass_data['email'] = filter_var($data['email'], FILTER_SANITIZE_STRING);
        $db->where ("contractor_no", $pass_data['contractor_no']);
        $db->where ("email", $pass_data['email']);
        $user = $db->getOne ("tbl_users");


        $mobileCode = random_int(1000,9999);
        $emailCode = md5($mobileCode);
        if($db->count>0){

            $data = Array (
                'contractor_no' => $pass_data['contractor_no'],
                'code' => $emailCode,
                'email' => $user['email'],
                'mobileCode' => $mobileCode,
                'confirmed' => '0',
                'createdAt' => $db->now(),
                'expires' => $db->now('+1d')
            );

            $db->insert("pass_request",$data);

            if($db->count>0){

                $emailFormat='changePassword';
                include 'emails/emails.php';

                $response = array(
                    'code'=>842,
                    'message'=>'Password Change requested please check your email('.$user['email'].')'
                );
                return json_encode($response, JSON_PRETTY_PRINT);

            }else {
                $response = array(
                    'code'=>843,
                    'message'=>'Failed to change password'
                );

            }}else{
            $response = array(
                'code'=>843,
                'message'=>'sorry User Not found'
            );
        }
        return json_encode($response, JSON_PRETTY_PRINT);


    }

    public function changePass($data){

        require 'config/dbconnect.php';

        //new salt Code//
        require_once 'config/keygen.php';

        $salt = getSalt();
        $salt = base64_encode($salt);
        $salt = str_replace('+', '.', $salt);

        $pass_data = [];
        $pass_data['newPassword'] = filter_var($data['newPassword'], FILTER_SANITIZE_STRING);
        $pass_data['code'] = filter_var($data['code'], FILTER_SANITIZE_STRING);
        $pass_data['email'] = filter_var($data['email'], FILTER_SANITIZE_STRING);
        $pass_data['salt'] = filter_var(@$salt, FILTER_SANITIZE_STRING);

        $salt = $pass_data['salt'];


        $db->where ("email", $pass_data['email']);
        $db->where ("code", $pass_data['code']);
        $db->where ("confirmed", '0');
        $db->getOne("pass_request");

        if($db->count>0){

            $db->where ("email", $pass_data['email']);
            $user = $db->getOne("tbl_users");

            if($db->count>0){

                $newPassword = crypt($pass_data['newPassword'], '$2y$10$'.$salt.'$');

                $confirmedData = array(
                    'confirmed'=>'1'
                );

                $db->where ("email", $pass_data['email']);
                $db->where ("code", $pass_data['code']);
                $db->where ("confirmed", '0');
                $db->update("pass_request",$confirmedData);

                if($db->count>0){
                    $passwordChange = array(
                        'userPass'=>$newPassword,
                        'salt'=>$salt,
                    );
                    $db->where ("email", $pass_data['email']);
                    $db->update("tbl_users",$passwordChange);

                    if($db->count>0){

                        $response = array(
                            'code'=>'015',
                            'contractor_no'=>$user['contractor_no'],
                            'message'=>'Password Changed you will be redirected to dashboard'
                        );
                        return json_encode($response, JSON_PRETTY_PRINT);
                    }
                    else{
                        $response = array(
                            'code'=>'016',
                            'message'=>'Sorry failed to change password'
                        );
                        return json_encode($response, JSON_PRETTY_PRINT);
                    }

                }
                else{

                    $response = array(
                        'code'=>'016',
                        'message'=>'Wrong Old Password'
                    );
                    return json_encode($response, JSON_PRETTY_PRINT);
                }

            }else{

            }

        }else{

            $response = array(
                'code'=>'013',
                'message'=>'Sorry password has been changed using this link please contact system admin'
            );
            return json_encode($response, JSON_PRETTY_PRINT);
        }


    }

    public function verifyPass($data){

        require 'config/dbconnect.php';

        $verifyPass_data = [];
        $merchant_id =$verifyPass_data['merchant_id'] = filter_var($data['merchant_id'], FILTER_SANITIZE_STRING);
        $code = $verifyPass_data['code'] = filter_var(@$data['code'], FILTER_SANITIZE_STRING);

        $db->where ('merchant_id', $merchant_id);
        $db->where ('mobileCode', $code);
        $db->where ('code', md5($code));

        $pass_request = $db->getOne ("pass_request");

        if($pass_request['merchant_id']==$merchant_id and $pass_request['confirmed']!=1){

            $passData = Array (
                'password' => $pass_request['password'],
                'salt' => $pass_request['salt'],
                'passwordChange' =>'0'
            );

            $db->where ('merchant_id', $pass_request['merchant_id']);
            $db->where ('passwordChange', '1');
            $db->update ('merchant', $passData);

            if($db->count>0)
            {
                $passReqData = Array (

                    'confirmed' =>'1'
                );
                $db->where ('merchant_id', $merchant_id);
                $db->where ('mobileCode', $code);
                $db->where ('code', md5($code));

                $db->update ('pass_request', $passReqData);

                $response = array(
                    "code"=>"113",
                    "message" => "Password Changed successfully");

            }else{

                $response = array(
                    "code"=>"112",
                    "message" => "Invalid code or code already verified, please try again");
            }


            return  json_encode($response,JSON_PRETTY_PRINT,True);

        }else{

            $response = array(
                "code"=>"112",
                "message" => "Invalid code or code already verified, please try again");

            return  json_encode($response,JSON_PRETTY_PRINT,True);
        }

    }

    public function verifyLink($data){

        require 'config/dbconnect.php';

        $link_data = [];
        $link_data['code'] = filter_var($data['code'], FILTER_SANITIZE_STRING);
        $link_data['email'] = filter_var($data['email'], FILTER_SANITIZE_STRING);

        $db->where ("email", $link_data['email']);
        $db->where ("code", $link_data['code']);
        $db->where ("confirmed", '0');
        $user = $db->getOne("pass_request");
        $userReg =  $user['email'];
        $expires=  $user['expires'];

        function passExpires($expires)
        {
            if(empty($expires)) {
                return "No date provided";
            }

            $periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
            $lengths         = array("60","60","24","7","4.35","12","10");

            $now             = time();
            $unix_date         = strtotime($expires);

            // check validity of date
            if(empty($unix_date)) {
                return "Bad date";
            }

            // is it future date or past date
            if($now > $unix_date) {
                $difference     = $now - $unix_date;
                $tense         = "ago";

            } else {
                $difference     = $unix_date - $now;
                $tense         = "from now";
            }

            for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
                $difference /= $lengths[$j];
            }

            $difference = round($difference);

            if($difference != 1) {
                $periods[$j].= "s";
            }

            return "$difference $periods[$j] {$tense}";
        }


        $expires = passExpires($expires);

        if($db->count>0){
            if (preg_match("/\bago\b/i", "$expires")) {

                $response = array(
                    'code'=>'012',
                    'message'=>'Sorry your link has expired :('
                );
                return json_encode($response, JSON_PRETTY_PRINT);
            } else {

                $response = array(
                    'code'=>'011',
                    'message'=>'Change password'
                );
                return json_encode($response, JSON_PRETTY_PRINT);
            }
        }else{
            $response = array(
                'code'=>'013',
                'message'=>'Sorry password has been changed using this link please contact system admin'
            );
            return json_encode($response, JSON_PRETTY_PRINT);

        }


    }

}