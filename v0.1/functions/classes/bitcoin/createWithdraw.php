<?php
/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/07/17
 * Time: 9:08 PM
 */

$input = new \BlockCypher\Api\TXInput();
$input->addAddress($bitcoin_address['address']);

/// Tx outputs
$output = new \BlockCypher\Api\TXOutput();
$output->addAddress($transaction_data['to_address']);
$output->setValue($SatoshiAmount); // Satoshis

/// Tx
$tx = new \BlockCypher\Api\TX();
$tx->addInput($input);
$tx->addOutput($output);

/// For Sample Purposes Only.
$request = clone $tx;

$txClient = new \BlockCypher\Client\TXClient($apiContexts[$server]);

try {
    $output = $txClient->create($tx);
} catch (Exception $ex) {;
    exit(1);
}

return $output;