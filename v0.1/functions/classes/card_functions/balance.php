<?php
/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/07/18
 * Time: 8:09 PM
 */

$webserviceCONFIG = new WebServiceConfiguration();
$setTerminalID = $webserviceCONFIG->setTerminalID();
$setWebURL = $webserviceCONFIG->setWebServiceURL();
$setTerminalPassword = $webserviceCONFIG->setTerminalPassword();
$transactionID = $OTP;
$currentDateAndTime = new TimeAndDateINiso8601();
$transactionDate = $currentDateAndTime->getCurrentTime();
$methodName = 'Balance';
$stringTobeHashed = $methodName.$setTerminalID.$profileNumber.$cardNumber.$transactionID.$transactionDate;
$checkSum = hash_hmac('sha1',$stringTobeHashed, $setTerminalPassword);

$request = xmlrpc_encode_request($methodName,

    array(
        $setTerminalID,
        $profileNumber,
        $cardNumber,
        $transactionID,
        date("Y-m-d H:i:s"),
        $checkSum,
    )
);

//create the stream context for the request
$context = stream_context_create(array('http' => array(
    'method' => "POST",
    'header' => "Content-Type: text/xml\r\nUser-Agent: PHPRPC/1.0\r\n",
    'content' => $request
)));

//URL of the XMLRPC Server
//$Cardserver = $setWebURL;
$file = file_get_contents($setWebURL, false, $context);
//decode the XMLRPC response
$responseBalance = xmlrpc_decode($file);