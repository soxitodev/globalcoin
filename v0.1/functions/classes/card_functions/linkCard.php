<?php

$webserviceCONFIG = new WebServiceConfiguration();
$setTerminalID = $webserviceCONFIG->setTerminalID();
$setWebURL = $webserviceCONFIG->setWebServiceURL();
$get_web_page = $webserviceCONFIG->get_web_page($setWebURL);
$setTerminalPassword = $webserviceCONFIG->setTerminalPassword();
$currentDateAndTime = new TimeAndDateINiso8601();
$transactionDate = $currentDateAndTime->getCurrentTime();
$methodName = 'LinkCard';
$stringTobeHashed = $methodName.$setTerminalID.$profileNumber.$data['cardNumber'].$transactionKey.$transactionDate;

$checkSum = hash_hmac('sha1',$stringTobeHashed, $setTerminalPassword);

$linkCardData =     array(
    $setTerminalID,
    $profileNumber,
    $data['cardNumber'],
    $transactionKey,
    date("Y-m-d H:i:s"),
    $checkSum,
);
$request = xmlrpc_encode_request($methodName,$linkCardData);


//create the stream context for the request
$context = stream_context_create(array('http' => array(
    'method' => "POST",
    'header' => "Content-Type: text/xml\r\nUser-Agent: PHPRPC/1.0\r\n",
    'content' => $request
)));

//URL of the XMLRPC Server
$server = $setWebURL;
$file = file_get_contents($server, false, $context);
//decode the XMLRPC response
$response = xmlrpc_decode($file);
//display the response

$errorLink = @$response['resultText'];


$dataLinked = array(

    'contractor_no'=>$data['contractor_no'],
    'cardNumber'=>$data['cardNumber'],
    'terminalID'=>$setTerminalID,
    'profileNumber'=>$profileNumber,

);

if($errorLink == 'Card already linked' or $errorLink == 'Invalid card number'){

} else{

    $db->insert('linkcard',$dataLinked);
}