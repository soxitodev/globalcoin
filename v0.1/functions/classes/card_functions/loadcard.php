<?php


//Checks  if request amount is above min required amount//

if($loadAmout<=49){

    $response = array(
        'code'=>'-7',
        'message'=>'Requested amount less than minimum load of R100 your current balance is R'.@$currentBalance.'',
    );
}
else {

    //If success transfare load card with funds//

        $webserviceCONFIG = new WebServiceConfiguration();
        $setTerminalID = $webserviceCONFIG->setTerminalID();
        $setWebURL = $webserviceCONFIG->setWebServiceURL();
        $get_web_page = $webserviceCONFIG->get_web_page($setWebURL);
        $setTerminalPassword = $webserviceCONFIG->setTerminalPassword();
        $transactionID = $OTP;
        $currentDateAndTime = new TimeAndDateINiso8601();
        $transactionDate = $currentDateAndTime->getCurrentTime();
        $methodName = 'LoadCardDeductProfile';
        $stringTobeHashed = $methodName . $setTerminalID . $profileNumber . $cardNumber . $loadAmout . '00' . $transactionKey . $transactionDate;
        $checkSum = hash_hmac('sha1', $stringTobeHashed, $setTerminalPassword);

        $dataAllocate = array(
            $setTerminalID,
            $profileNumber,
            $cardNumber,
            $loadAmout . '00',
            $transactionKey,
            date("Y-m-d H:i:s"),
            $checkSum,
        );
        $request = xmlrpc_encode_request($methodName, $dataAllocate);

//create the stream context for the request
        $context = stream_context_create(array('http' => array(
            'method' => "POST",
            'header' => "Content-Type: text/xml\r\nUser-Agent: PHPRPC/1.0\r\n",
            'content' => $request
        )));

        $file = file_get_contents($setWebURL, false, $context);
//decode the XMLRPC response
        $responseLoad = xmlrpc_decode($file);

    if($responseLoad['resultText']=='OK'){

        $withdrawData = [
            'ref'=>$transaction_data['ref'],
            'contractor_no'=>$transaction_data['contractor_no'],
            'cardNumber'=>$responseLoad['cardNumber'],
            'balanceAmount'=>$responseLoad['balanceAmount'],
            'requestAmount'=>$responseLoad['requestAmount'],
            'transactionFee'=>$responseLoad['transactionFee'],
            'serverTransactionID'=>$responseLoad['serverTransactionID'],
            'loadedAt'=>$db->NOW()
        ];
        //Stores the transaction to avoid duplication//
        $db->insert('loadcard',$withdrawData);
    }


}

?>