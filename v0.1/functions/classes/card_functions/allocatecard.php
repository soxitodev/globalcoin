<?php

        $webserviceCONFIG = new WebServiceConfiguration();
        $setTerminalID = $webserviceCONFIG->setTerminalID();
        $setWebURL = $webserviceCONFIG->setWebServiceURL();
        $get_web_page = $webserviceCONFIG->get_web_page($setWebURL);
        $setTerminalPassword = $webserviceCONFIG->setTerminalPassword();
        $transactionID = $OTP;
        $currentDateAndTime = new TimeAndDateINiso8601();
        $transactionDate = $currentDateAndTime->getCurrentTime();
        $methodName = 'AllocateCard';
        $stringTobeHashed = $methodName.$setTerminalID.$profileNumber.$data['cardNumber'].$data['first_name'].$data['last_name'].$data['id_no'].
            $data['phone'].$transactionKey.$transactionDate;
        $checkSum = hash_hmac('sha1',$stringTobeHashed, $setTerminalPassword);

        $dataAllocate =   array(
            $setTerminalID,
            $profileNumber,
            $data['cardNumber'],
            $data['first_name'],
            $data['last_name'],
            $data['id_no'],
            $data['phone'],
            $transactionKey,
            date("Y-m-d H:i:s"),
            $checkSum,
        );
        $request = xmlrpc_encode_request($methodName,$dataAllocate );

//create the stream context for the request
        $context = stream_context_create(array('http' => array(
            'method' => "POST",
            'header' => "Content-Type: text/xml\r\nUser-Agent: PHPRPC/1.0\r\n",
            'content' => $request
        )));

//URL of the XMLRPC Server
        $server = $setWebURL;
        $file = file_get_contents($server, false, $context);
//decode the XMLRPC response
        $response = xmlrpc_decode($file);

        $errorAllocate = @$response['resultCode'];

if($errorAllocate == '-19'){

    $response = array(
        'code'=>'119',
        'message'=>$response['resultText']
    );

}elseif($errorAllocate == '-2')
{
    $response = array(
        'code'=>'120',
        'message'=>$response['resultText']
    );
}else{
    $successData = array(
                'contractor_no'=>$data['cardNumber'],
                'cellNo'=>$data['phone'],
                'id_pass_no'=>$data['id_no'],
                'first_name'=>$data['first_name'],
                'last_name'=>$data['last_name'],
                'serverTransactionID'=>$response['serverTransactionID'],
                'clientTransactionID'=>$response['clientTransactionID'],
                'cardNumber'=>$data['cardNumber']
    );
    $db->insert('allocatecard',$successData);
}




?>