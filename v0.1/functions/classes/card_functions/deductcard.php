<?php

if($data['requestAmount']<=99){

    $response = array(
        'code'=>'-7',
        'message'=>'Requested amount less than minimum load of R100'
    );
}else{
    $webserviceCONFIG = new WebServiceConfiguration();
    $setTerminalID = $webserviceCONFIG->setTerminalID();
    $setWebURL = $webserviceCONFIG->setWebServiceURL();
    $get_web_page = $webserviceCONFIG->get_web_page($setWebURL);
    $setTerminalPassword = $webserviceCONFIG->setTerminalPassword();
    $transactionID = $OTP;
    $currentDateAndTime = new TimeAndDateINiso8601();
    $transactionDate = $currentDateAndTime->getCurrentTime();
    $methodName = 'DeductCardLoadProfile';
    $stringTobeHashed = $methodName.$setTerminalID.$profileNumber.$data['cardNumber'].$data['requestAmount'].'00'.$transactionKey.$transactionDate;
    $checkSum = hash_hmac('sha1',$stringTobeHashed, $setTerminalPassword);

    $dataAllocate =   array(
        $setTerminalID,
        $profileNumber,
        $data['cardNumber'],
        $data['requestAmount'].'00',
        $transactionKey,
        date("Y-m-d H:i:s"),
        $checkSum,
    );
    $request = xmlrpc_encode_request($methodName,$dataAllocate );

//create the stream context for the request
    $context = stream_context_create(array('http' => array(
        'method' => "POST",
        'header' => "Content-Type: text/xml\r\nUser-Agent: PHPRPC/1.0\r\n",
        'content' => $request
    )));

//URL of the XMLRPC Server
    $server = $setWebURL;
    $file = file_get_contents($server, false, $context);
//decode the XMLRPC response
    $response = xmlrpc_decode($file);

    $errorAllocate = @$response['resultCode'];

    $response = array(
        'code'=>'-7',
        'message'=>$response
    );
}

?>