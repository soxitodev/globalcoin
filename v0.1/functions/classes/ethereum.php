<?php

/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/07/14
 * Time: 2:02 PM
 */
class ethereum
{
    public function address($label){

        require 'config/dbconnect.php';
        require 'config/blockcypher.php';

        $colls = array('address');
        $db->where ("contractor_no", $label);
        $ethereum_address = $db->getOne ("ethereum_address",$colls);


        if($db->count>0){

            $ADDRESS = $ethereum_address['address'];

            $result = $apiEth->get("addrs/$ADDRESS/balance");

            $response = [
                'code'=>'1000',
                'ethereum_data'=>json_decode($result->response),
            ];

        }else{

            $result = $apiEth->post("addrs?$token");

            $address_data = json_decode($result->response);

            //bitcoin_address

            $data = Array(
                'contractor_no' => $label,
                'private' => $address_data->private,
                'public' => $address_data->public,
                'address' => $address_data->address
            );

            $db->insert('ethereum_address', $data);

            if($db->count>0){

                $response = [
                    'code'=>'1001',
                    'message'=>'etherium address created',
                ];
            }

        }


        return json_encode($response);
    }
}