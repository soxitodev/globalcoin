<?php

/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/07/17
 * Time: 11:13 AM
 */
class bloccypher
{
public function address($data){

    require 'config/dbconnect.php';
    include 'config/blockcypher.php';
    require 'config/config.php';


    $addressClient = new \BlockCypher\Client\AddressClient($apiContexts[$server]);

    $address_data = [];
    $address_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);

    $db->where ("contractor_no", $address_data['contractor_no']);
    $bitcoin_address = $db->getOne ("bitcoin_address");

    if($db->count>0){

        $addressBalance = $apiBalance ->get('get_address_balance/'.$network.'/'.$bitcoin_address['address'].'');

        $db->where ("contractor_no_from", $data['contractor_no']);
        $db->where ("status", 'Pending');

        $pending = $db->get("sell_coins",null,['coins']);
        $amount=0;
        foreach ($pending as $i =>$payments){

            $amount += $pending[$i]['coins'];

        }

        $available_balance = round($addressBalance['data']->confirmed_balance - $amount,8);

        $bitcoin_info=[
            'address'=>$addressBalance['data']->address,
            'network'=>$addressBalance['data']->network,
            'balance'=>$addressBalance['data']->confirmed_balance,
            'unconfirmed_balance'=>$addressBalance['data']->unconfirmed_balance,
            'available_balance'=>negativeToZero($available_balance),
            'pending_payments'=>$amount,
            'final_balance'=>$available_balance+$addressBalance['data']->unconfirmed_balance,
        ];

        $response = [
            'code'=>'695',
            'address_data'=>$bitcoin_info
        ];
    }
        else{

// ### Create Address
    $addressKeyChain = $addressClient->generateAddress();

    if($addressKeyChain->private!=null){
        //bitcoin_address
        $AddressData =[
            'contractor_no' =>$address_data['contractor_no'],
            'private' =>$addressKeyChain->private,
            'public' =>$addressKeyChain->public,
            'address' =>$addressKeyChain->address,
            'network' =>$network,
            'wif' =>$addressKeyChain->wif,
            'label' =>$address_data['contractor_no']
        ];

        $db->insert('bitcoin_address', $AddressData);

        if($db->count>0){

            $response = [
                'message'=>'Address Created'
            ];

        }


        }
    }

    return json_encode($response);
}
public function createTransaction($data){
    require 'config/dbconnect.php';
    include 'config/blockcypher.php';
    require 'config/config.php';

    $transaction_data = [];
    $transaction_data['ref'] = filter_var($data['ref'], FILTER_SANITIZE_STRING);
    $transaction_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);
    $transaction_data['requestAmount'] = filter_var($data['requestAmount'], FILTER_SANITIZE_STRING);
    $transaction_data['to_address'] = filter_var($data['to_address'], FILTER_SANITIZE_STRING);

    //Checks if payment has been Processed//
    $db->where ("contractor_no_from", $transaction_data['contractor_no']);
    $db->where ("address_to", $transaction_data['to_address']);
    $db->where ("ref", $transaction_data['ref']);
    $db->where ("coins", $transaction_data['requestAmount']);
    $db->where ("status", 'Processed');
    $db->get ("sell_coins");
    if($db->count>0){

        $response = array(
            'code'=>'510',
            'message'=>'Transaction already processed'
        );
    }

    else{
    $db->where ("contractor_no", $transaction_data['contractor_no']);
    $bitcoin_address = $db->getOne ("bitcoin_address");

    $network_fee = $block_io->get_network_fee_estimate(array('amounts' => $transaction_data['requestAmount'], 'to_addresses' => $transaction_data['to_address']));

    $balance = $apiBalance ->get('get_address_balance/'.$network.'/'.$bitcoin_address['address'].'');

    $estimated_network_fee = $network_fee->data->estimated_network_fee;
    $requestAmount = $transaction_data['requestAmount'];
    $coins = $requestAmount;
    $total = round_up($coins,8);
    //If totall is available is greater send funds//
   if($balance['status']=='fail'){
       $response =[
            'message'=>$balance->data->error_message,
        ];
    }
    else{

    if($balance['data']->confirmed_balance>$total){

        $SatoshiAmount = round(convertToSatoshiFromBTC($transaction_data['requestAmount']));

     $txSkeleton = require 'bitcoin/createTransaction.php';

         $txClient = new \BlockCypher\Client\TXClient($apiContexts[$server]);

       $privateKeys = array(
           $bitcoin_address['private'] // Address: n3D2YXwvpoPg8FhcWpzJiS3SvKKGD8AXZ4
       );

/// Sign TXSkeleton
       $txSkeleton = $txClient->sign($txSkeleton, $privateKeys);

/// For sample purposes only.
       $request = clone $txSkeleton;

        try {
            /// Send TX to the network
            $txSkeleton = $txClient->send($txSkeleton);

            $data=json_decode($txSkeleton->tx);

            $success_txData = [
                'block_height'=>$data->block_height,
                'block_index'=>$data->block_index,
                'hash'=>$data->hash,
                'addresses'=>json_encode($data->addresses),
                'total'=>$data->total,
                'fees'=>$data->fees,
                'size'=>$data->size,
                'preference'=>$data->preference,
                'relayed_by'=>$data->relayed_by,
                'received'=>$data->received,
                'ver'=>$data->ver,
                //'lock_time'=>$data->lock_time,
                'double_spend'=>$data->double_spend,
                'vin_sz'=>$data->vin_sz,
                'vout_sz'=>$data->vout_sz,
                'confirmations'=>$data->confirmations,
                'inputs'=>json_encode($data->inputs),
                'outputs'=>json_encode($data->outputs)
            ];

            $db->insert('success_tx',$success_txData);

            if($db->count>0){


                $success = array(
                    'status'=>'Processed'
                );
                $db->where ("contractor_no_from", $transaction_data['contractor_no']);
                $db->where ("address_to", $transaction_data['to_address']);
                $db->where ("ref", $transaction_data['ref']);
                $db->where ("coins", $transaction_data['requestAmount']);
                $db->update ("sell_coins",$success);

                if($db->count>0){

                    $db->where ("ref", $transaction_data['ref']);
                    $contractor = $db->getOne ("sell_coins");

                    //Gets the user to send notification//
                    $db->where ("contractor_no", $contractor['contractor_no_to']);
                    $cols = array('email','first_name');
                    $user = $db->getOne("tbl_users",$cols);
                    $emailFormat = 'paymentSent';

                    include 'emails/emails.php';
                    //Checks if mail is sent//
                    if(!$mail->send()){
                        $response = array(
                            'code'=>328,
                            'message'=>'Mail error',
                            'mail'=>$mail->ErrorInfo
                        );
                    }
                    else{
                        $response =[
                            'code'=>'509',
                            'message'=>'Coins sent successfully',
                        ];
                    }
                }

            }else{

                $response =[
                    'message'=>'failed to process transaction',

                ];
            }

        }
        catch (Exception $ex) {

            exit(1);
        }
        /*
            $withdrawAmount = round($price * $transaction_data['requestAmount']);*/

    }
    else{
        $response =[
            'message'=>'trade could not be sent due to insufficient funds, network fee of '.$estimated_network_fee.' is required'
        ];
    }
    }
    }
return json_encode($response);
}
public function withDraw($data){

    require 'config/dbconnect.php';
    require 'config/keygen.php';
    require 'config/blockcypher.php';
    require 'config/config.php';

    $transaction_data = [];
    $transaction_data['ref'] = filter_var($data['ref'], FILTER_SANITIZE_STRING);
    $transaction_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);
    $transaction_data['requestAmount'] = filter_var($data['requestAmount'], FILTER_SANITIZE_STRING);
    $transaction_data['to_address'] = filter_var($data['to_address'], FILTER_SANITIZE_STRING);

    $db->where ("contractor_no", $transaction_data['contractor_no']);
    $db->where ("ref", $transaction_data['ref']);
    $db->getOne ("loadcard");

    if($db->count>0){
        $resposne =[
            'message'=>'Transaction already sent please create a new transaction',

        ];
    }else{
    $db->where ("contractor_no", $transaction_data['contractor_no']);
    $bitcoin_address = $db->getOne ("bitcoin_address");


    $balance = $apiBalance ->get('get_address_balance/'.$network.'/'.$bitcoin_address['address'].'');
    $total = $transaction_data['requestAmount'];

    //If totall is available is greater send funds//
   if($balance['status']=='fail'){
        $resposne =[
            'message'=>$balance->data->error_message,
        ];
    }else{
    if($balance['data']->confirmed_balance>$total){

        $db->where ("contractor_no", $transaction_data['contractor_no']);
        $cardInfo =  $db->getOne("allocatecard",['cardNumber']);
        $cardNumber = $cardInfo['cardNumber'];

        //Gets the card Balance//
        require 'card_functions/balance.php';

        if($responseBalance['balanceAmount']<500000 and $responseBalance['expired']=='NO'){

        $SatoshiAmount = round(convertToSatoshiFromBTC($transaction_data['requestAmount']));

        $loadAmout = round($price*$transaction_data['requestAmount']);
        $txSkeleton = require 'bitcoin/createWithdraw.php';

        $txClient = new \BlockCypher\Client\TXClient($apiContexts[$server]);

        $privateKeys = array(
            $bitcoin_address['private'] // Address: n3D2YXwvpoPg8FhcWpzJiS3SvKKGD8AXZ4
        );
/// Sign TXSkeleton
        $txSkeleton = $txClient->sign($txSkeleton, $privateKeys);

/// For sample purposes only.
        $request = clone $txSkeleton;

        try {
            /// Send TX to the network
            $txSkeleton = $txClient->send($txSkeleton);

            $data=json_decode($txSkeleton->tx);

            $success_txData = [
                'block_height'=>$data->block_height,
                'block_index'=>$data->block_index,
                'hash'=>$data->hash,
                'addresses'=>json_encode($data->addresses),
                'total'=>$data->total,
                'fees'=>$data->fees,
                'size'=>$data->size,
                'preference'=>$data->preference,
                'relayed_by'=>$data->relayed_by,
                'received'=>$data->received,
                'ver'=>$data->ver,
                'lock_time'=>$data->lock_time,
                'double_spend'=>$data->double_spend,
                'vin_sz'=>$data->vin_sz,
                'vout_sz'=>$data->vout_sz,
                'confirmations'=>$data->confirmations,
                'inputs'=>json_encode($data->inputs),
                'outputs'=>json_encode($data->outputs)
            ];
            $db->insert('success_tx',$success_txData);

            if($db->count>0){

                require_once 'card_functions/loadcard.php';

                if($responseLoad['resultText']=='OK'){

                    $balanceAmount = substr_replace(@$responseLoad['balanceAmount'], "", -2);
                    $resposne =[
                        'message'=>'Card Loaded with R'.$loadAmout.'.00 current balance amount is R'.$balanceAmount.'.00',
                        'load'=>$withdrawData
                    ];
                }else{

                    $resposne =[
                        'message'=>'failed to Load Card with funds',

                    ];
                }

            }else{

                $resposne =[
                    'message'=>'failed to process transaction',

                ];
            }

        }

        catch (Exception $ex) {

            exit(1);
        }

    }
    else{
        $resposne =[
            'message'=>'card could not be loaded please check your card status on the wallet'
        ];
    }
    }
    else{
        $resposne =[
            'message'=>'insufficient funds'
        ];
    }
    }}
return json_encode($resposne);

}
public function balance($label){

    require 'config/dbconnect.php';
    include 'config/blockcypher.php';

    $db->where ("contractor_no", $label);
    $bitcoin_address = $db->getOne ("bitcoin_address");

    $addressClient = new \BlockCypher\Client\AddressClient($apiContexts[$server]);

    $addressBalance = $addressClient->getBalance($bitcoin_address['address']);

    $response =[
        'address'=>$addressBalance->address,
        'total_received'=>$addressBalance->total_received,
        'total_sent'=>$addressBalance->total_sent,
        'balance'=>$addressBalance->balance,
        'unconfirmed_balance'=>$addressBalance->unconfirmed_balance,
        'final_balance'=>$addressBalance->final_balance,
    ];

    return json_encode($response);

}
}