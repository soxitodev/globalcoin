<?php

/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/06/21
 * Time: 11:24 AM
 */

class bitcoins{

    private $lable = '';


    public function address($data){

        require 'config/dbconnect.php';
        include 'config/blockcypher.php';
        require 'config/config.php';


        $addressClient = new \BlockCypher\Client\AddressClient($apiContexts[$server]);

        $address_data = [];
        $address_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);

        $db->where ("contractor_no", $address_data['contractor_no']);
        $bitcoin_address = $db->getOne ("bitcoin_address");

        if($db->count>0){

            $addressBalance = $addressClient->getBalance($bitcoin_address['address']);

            $db->where ("contractor_no_from", $bitcoin_address['address']);
            $db->where ("status", 'Pending');

            $pending = $db->get("sell_coins",null,['coins']);
            $amount=0;
            foreach ($pending as $i =>$payments){

                $amount += $pending[$i]['coins'];

            }

            $available_balance = round(convertToBTCFromSatoshi($addressBalance->balance) - $amount,8);

            $bitcoin_info=[
                'address'=>$addressBalance->address,
                'network'=>$bitcoin_address['network'],
                'total_received'=>formatBTC(convertToBTCFromSatoshi($addressBalance->total_received)),
                'total_sent'=>formatBTC(convertToBTCFromSatoshi($addressBalance->total_sent)),
                'balance'=>formatBTC(convertToBTCFromSatoshi($addressBalance->balance)),
                'unconfirmed_balance'=>formatBTC(convertToBTCFromSatoshi($addressBalance->unconfirmed_balance)),
                'available_balance'=>$available_balance,
                'pending_payments'=>$amount,
                'final_balance'=>formatBTC(convertToBTCFromSatoshi($addressBalance->final_balance)-$available_balance),
            ];

            $response = [
                'code'=>'695',
                'address'=>$bitcoin_info
            ];
        }
        else{

// ### Create Address
            $addressKeyChain = $addressClient->generateAddress();

            if($addressKeyChain->private!=null){
                //bitcoin_address
                $AddressData =[
                    'contractor_no' =>$address_data['contractor_no'],
                    'private' =>$addressKeyChain->private,
                    'public' =>$addressKeyChain->public,
                    'address' =>$addressKeyChain->address,
                    'network' =>$network,
                    'wif' =>$addressKeyChain->wif,
                    'label' =>$address_data['contractor_no']
                ];

                $db->insert('bitcoin_address', $AddressData);

                if($db->count>0){

                    $response = [
                        'message'=>'Address Created'
                    ];

                }


            }
        }

        return json_encode($response);
    }
    public function getBalance($lable){
        require 'config/dbconnect.php';
        include 'config/blockcypher.php';
        require 'config/config.php';

        $this->lable = $lable;
        $db->where ("contractor_no", $this->lable);

        $bitcoin_address = $db->getOne ("bitcoin_address");

        if($db->count>0){

           // $balance = $block_io->get_address_by_label(array('label' => $this->lable));
            $balance =  $block_io->get_address_balance(array('addresses' => $bitcoin_address['address']));

            $db->where ("contractor_no_from", $this->lable);
            $db->where ("status", 'Pending');

            $pending = $db->get("sell_coins",null,['coins']);

            $amount=0;
            foreach ($pending as $i =>$payments){

                $amount += $pending[$i]['coins'];

            }
            $available_balance = $balance->data->available_balance - $amount;
            $response = [
                'network'=>$balance->data->network,
                'address'=>$bitcoin_address['address'],
                'balance'=>$balance->data->available_balance,
                'available_balance'=>$available_balance,
                'pending_payments'=>$amount,
                'pending_received_balance'=>$balance->data->pending_received_balance,

            ];
            return json_encode($response, JSON_PRETTY_PRINT);

        }
        else{

            $newAddressInfo = $block_io->get_new_address(array('label' => $this->lable));

            if($newAddressInfo->status=='success'){
                //Stores bitcoin_address//
                $data = Array(
                    'contractor_no' => $this->lable,
                    'network' => $newAddressInfo->data->network,
                    'address' => $newAddressInfo->data->address,
                    'label' => $newAddressInfo->data->label
                );

                $db->insert('bitcoin_address', $data);

                if($db->count>0){

                    $response = array(

                        'status'=>$newAddressInfo->status,
                        'message'=> 'BTC Address created, your address is '.$newAddressInfo->data->address.''

                    );

                }


            }else{
               $response = $newAddressInfo;
            }
        }

        return json_encode($response, JSON_PRETTY_PRINT);
    }
    public function withDraw($data){
        require 'config/dbconnect.php';
        require 'config/config.php';
        $withDraw_data = [];
        $withDraw_data['label'] = filter_var($data['label'], FILTER_SANITIZE_STRING);
        $amount = $withDraw_data['amount'] = filter_var($data['amount'], FILTER_SANITIZE_STRING);

        $this->lable = $withDraw_data['label'];

        $db->where ("contractor_no", $this->lable);
        $bitcoin_address = $db->getOne ("bitcoin_address");

        $fee = $amount * 0.0001;

        $total = $amount + 0.00042432 + $fee;

        $balance =  $block_io->get_address_balance(array('addresses' => $bitcoin_address['address']));

        if($balance->data->available_balance>$total){

            $withdraw = $block_io->withdraw_from_addresses(array('amounts' => $amount,$fee, 'from_addresses' => $bitcoin_address['address'],$bitcoin_address['address'], 'to_addresses' => $toAddress,$toAddress));
            //---------------Checks If Transaction is Successful---------------------//
            if($withdraw->status =='fail'){

                $data = array(
                    'status'=>$withdraw->status,
                    'message'=>$withdraw->data
                );

                return json_encode($data, JSON_PRETTY_PRINT);
            }
            else{

                //-----------Load Card with Funds---------------//

                if(round($total*$zar, 2)>1000){

                    $db->where ("contractor_no", $withDraw_data['label']);
                    $getCard = $db->getOne ("allocatecard");
                    $cardNumberWithdraw = $getCard['cardNumber'];

                    //---------------Load Card Call---------------//
                    $webserviceCONFIG = new WebServiceConfiguration();
                    $setTerminalID = $webserviceCONFIG->setTerminalID();
                    $setWebURL = $webserviceCONFIG->setWebServiceURL();
                    $setTerminalPassword = $webserviceCONFIG->setTerminalPassword();
                    $transactionID = $OTP;
                    $requestAmount = $amount;
                    $currentDateAndTime = new TimeAndDateINiso8601();
                    $transactionDate = $currentDateAndTime->getCurrentTime();
                    $methodName = 'LoadCardDeductProfile';

                    $stringTobeHashed = $methodName.$setTerminalID.$profileNumber.$cardNumberWithdraw.$requestAmount."00".$transactionID.$transactionDate;

                    $checkSum = hash_hmac('sha1',$stringTobeHashed, $setTerminalPassword);

                    $request = xmlrpc_encode_request($methodName,
                        $data = array(
                            $setTerminalID,
                            $profileNumber,
                            $cardNumberWithdraw,
                            $requestAmount."00",
                            $transactionID,
                            date("Y-m-d H:i:s"),
                            $checkSum,
                        )
                    );


//create the stream context for the request
                    $context = stream_context_create(array('http' => array(
                        'method' => "POST",
                        'header' => "Content-Type: text/xml\r\nUser-Agent: PHPRPC/1.0\r\n",
                        'content' => $request
                    )));

//URL of the XMLRPC Server
                    $server = $setWebURL;
                    $file = file_get_contents($server, false, $context);
//decode the XMLRPC response
                    $response = xmlrpc_decode($file);
//display the response

                    $amount = $response['balanceAmount'];
                    $requestAmount = $response['requestAmount'];
                    $cardNumber = $response['cardNumber'];
                    $balanceAmount = $response['balanceAmount'];
                    $serverTransactionID = $response['serverTransactionID'];
                    $transactionFee = $response['transactionFee'];
                    $resultText = $response['resultText'];

                    $dataLoad = array(
                        'contractor_no'=>$contractor,
                        'cardNumber'=>$cardNumber,
                        'balanceAmount'=>$balanceAmount,
                        'requestAmount'=>$requestAmount,
                        'serverTransactionID'=>$serverTransactionID,
                        'transactionFee'=>$fee
                    );

                    if($resultText == 'OK'){

                        $db->insert('loadcard',$dataLoad);
                        if($db->count>0){

                            $response = array(
                                'code'=>902,
                                'message'=>'card loaded Successfully'
                            );
                        }

                    }

                }


            }
        }
        else{
            $messege = array(
                'status'=>'fail',
                'message'=>'Cannot withdraw funds without max Fee of '.$total.' BTC',
                'available_amount'=>$balance->data->available_balance
            );

            return json_encode($messege, JSON_PRETTY_PRINT);

        }

    }
    public function createAddress($data){

        require 'config/dbconnect.php';
        $address_data = [];
        $address_data['label'] = filter_var($data['label'], FILTER_SANITIZE_STRING);

        require 'config/config.php';

        $newAddressInfo = $block_io->get_new_address(array('label' => $address_data['label']));

        if($newAddressInfo->status=='success'){

            //bitcoin_address

            $data = Array(
                'contractor_no' => $address_data['label'],
                'network' => $newAddressInfo->data->network,
                'address' => $newAddressInfo->data->address,
                'label' => $newAddressInfo->data->label
            );

            $db->insert('bitcoin_address', $data);

            if($db->count>0){

                $data = array(

                    'status'=>$newAddressInfo->status,
                    'message'=> 'BTC Address created, your address is '.$newAddressInfo->data->address.''

                );
                return json_encode($data, JSON_PRETTY_PRINT);
            }


        }else{
            return json_encode($newAddressInfo);
        }



    }
    public function transfareCoins($data){
        require 'config/dbconnect.php';
        require 'config/config.php';

        $withDraw_data = [];
        $withDraw_data['ref'] = filter_var($data['ref'], FILTER_SANITIZE_STRING);
        $withDraw_data['label'] = filter_var($data['label'], FILTER_SANITIZE_STRING);
        $withDraw_data['address_to'] = filter_var($data['address_to'], FILTER_SANITIZE_STRING);
        $amount = $withDraw_data['coins'] = filter_var($data['coins'], FILTER_SANITIZE_STRING);

        //Checks if payment has been Processed//
        $db->where ("contractor_no_from", $withDraw_data['label']);
        $db->where ("address_to", $withDraw_data['address_to']);
        $db->where ("ref", $withDraw_data['ref']);
        $db->where ("coins", $withDraw_data['coins']);
        $db->where ("status", 'Processed');
        $db->get ("sell_coins");
        if($db->count>0){
            $response = array(
                'code'=>'510',
                'message'=>'Transaction already processed'
            );
        }

        else{
        //fees//


        $transfee = $amount*0.01;
        $fee =  0.00010 + $transfee;
        $networkfee = 0.00042432*2;
        $total = $amount + $networkfee + $fee;

        $balance  = $block_io->get_address_balance(array('labels' => $withDraw_data['label']));

        if($balance->data->available_balance>$total){

            $withdraw = $block_io->withdraw_from_labels(array('amounts' => $amount, 'from_labels' => $withDraw_data['label'], 'to_addresses' => $withDraw_data['address_to']));

            $fee = $block_io->withdraw_from_labels(array('amounts' => $fee, 'from_labels' => $withDraw_data['label'], 'to_addresses' => $toAddress));

            if($withdraw->status =='fail'){

                $response = array(
                    'code'=>'510',
                    'status'=>$withdraw->status,
                    'message'=>$withdraw->data
                );

            }
            else{

                $success = array(
                    'status'=>'Processed'
                );
                $db->where ("contractor_no_from", $withDraw_data['label']);
                $db->where ("address_to", $withDraw_data['address_to']);
                $db->where ("ref", $withDraw_data['ref']);
                $db->where ("coins", $withDraw_data['coins']);
                $db->update ("sell_coins",$success);

                if($db->count>0){

                    $db->where ("ref", $withDraw_data['ref']);
                    $contractor = $db->getOne ("sell_coins");

                    //Gets the user to send notification//
                    $db->where ("contractor_no", $contractor['contractor_no_to']);
                    $cols = array('email','first_name');
                    $user = $db->getOne("tbl_users",$cols);
                    $emailFormat = 'paymentSent';

                    include 'emails/emails.php';
                    //Checks if mail is sent//
                    if(!$mail->send()){
                        $response = array(
                            'code'=>328,
                            'message'=>'Mail error',
                            'mail'=>$mail->ErrorInfo
                        );
                    }
                    else{

                        $data = array(
                            'network'=>$withdraw->data->network,
                            'txid'=>$withdraw->data->txid,
                            'amount_withdrawn'=>$withdraw->data->amount_withdrawn,
                            'amount_sent'=>$withdraw->data->amount_sent,
                            'network_fee'=>$withdraw->data->network_fee,
                            'blockio_fee'=>$withdraw->data->blockio_fee,
                        );
                        $response = array(
                            'code'=>'509',
                            'message'=>'Coins sent successfully'
                        );
                    }
                }
            }
        }
        else{
            $response = array(
                'code'=>'510',
                'status'=>'fail',
                'message'=>'Cannot send funds without max Fee of '.$total.' BTC',
                'data'=>$data,
                'available_amount'=>$balance->data->available_balance
            );

        }
   }

        return json_encode($response, JSON_PRETTY_PRINT);

    }
    public function createTrade($data){
        $trade_data = [];
        $trade_data['contractor_no'] = filter_var($data['contractor_no'], FILTER_SANITIZE_STRING);
        $trade_data['payment_method'] = filter_var($data['payment_method'], FILTER_SANITIZE_STRING);
        $trade_data['price'] = filter_var($data['price'], FILTER_SANITIZE_STRING);
        $trade_data['min_trade_limit'] = filter_var($data['min_trade_limit'], FILTER_SANITIZE_STRING);
        $trade_data['max_trade_limit'] = filter_var($data['max_trade_limit'], FILTER_SANITIZE_STRING);
        $trade_data['location'] = filter_var($data['location'], FILTER_SANITIZE_STRING);
        $trade_data['payment_window'] = filter_var($data['payment_window'], FILTER_SANITIZE_STRING);

        require 'config/dbconnect.php';
        require 'config/config.php';
        //create_trade
         $db->where ("contractor_no", $trade_data['contractor_no']);
         $db->getOne ("create_trade");

        if($db->count>0){

             $db->where ("contractor_no", $trade_data['contractor_no']);
             $db->where ("network", @$network);

             $address = $db->getOne ("bitcoin_address");

             $dataTrade = Array(
                 'contractor_no' => $trade_data['contractor_no'],
                 'payment_method' => $trade_data['payment_method'],
                 'price' => $trade_data['price'],
                 'min_trade_limit' => $trade_data['min_trade_limit'],
                 'max_trade_limit' => $trade_data['max_trade_limit'],
                 'location' => $trade_data['location'],
                 'payment_window' => $trade_data['payment_window'],
                 'bit_address' => $address['address'],
                 'createdAt' => $db->now()
             );
             if($db->count>0){
                 $db->where ("contractor_no", $trade_data['contractor_no']);
                 $db->update ("create_trade",$dataTrade);

                 if($db->count>0){

                     $response = array(
                         'code'=>541,
                         'message'=>'Trade Updated'
                     );
                 }
                 else{

                     $response = array(
                         'code'=>542,
                         'message'=>'Nothing to Update');
                 }}

             return json_encode($response, JSON_PRETTY_PRINT);
         }
         else{

             $db->where ("contractor_no", $trade_data['contractor_no']);
             $db->where ("network", @$network);
             $address = $db->getOne ("bitcoin_address");

             $dataTrade = Array(
                 'contractor_no' => $trade_data['contractor_no'],
                 'payment_method' => $trade_data['payment_method'],
                 'price' => $trade_data['price'],
                 'min_trade_limit' => $trade_data['min_trade_limit'],
                 'max_trade_limit' => $trade_data['max_trade_limit'],
                 'location' => $trade_data['location'],
                 'payment_window' => $trade_data['payment_window'],
                 'bit_address' => $address['address'],
                 'createdAt' => $db->now()
             );

             $db->insert('create_trade', $dataTrade);
             if($db->count>0){
                 $response = array(
                     'code'=>540,
                     'message'=>'Trade Created'
                 );

                 return json_encode($response, JSON_PRETTY_PRINT);}
         }


        return json_encode($response);

    }
    public function getSellers($data){
        require 'config/dbconnect.php';

        $resellers=[];

        $sellers_data = [];
        $sellers_data['location'] = filter_var($data['location'], FILTER_SANITIZE_STRING);

        $colls = array('contractor_no','first_name','last_name','email','phone');
        $db->where ("can_sell", 'Yes');
        $sellers  = $db->get ("tbl_users",null,$colls);

        foreach ($sellers as $r => $users){

            $collsTrade = array('contractor_no','payment_method','price','min_trade_limit','max_trade_limit','location','payment_window','bit_address');
            $db->where ("location", $sellers_data['location']);
            $db->where ("contractor_no", $users['contractor_no']);
            $create_trade = $db->getOne ("create_trade",$collsTrade);

            $collsBankinfo = array('bank_name','currency_code','bank_account','account_type','account_holder');
            $db->where ("contractor_no", $sellers[$r]['contractor_no']);
            $bankingDetails = $db->getOne ("bank_account",$collsBankinfo);

            if($db->count>0){
                $create_trade =$create_trade;
            }else{
                $create_trade ='no trades added';
            }
            $resellers[] = [
                'first_name'=>$users['first_name'],
                'last_name'=>$users['last_name'],
                'email'=>$users['email'],
                'phone'=>$users['phone'],
                'bankinfo'=>$bankingDetails,
                'buy_coin'=>$create_trade,
                ];
        }

       include 'bitcoin/resellers.php';
        $response = array(
            'code'=>'432',
            'reseller'=>$resellers

        );
        return json_encode($response, JSON_PRETTY_PRINT);
    }
    public function submitSale($data){
        $sale_data = [];
        $sale_data['contractor_no_from'] = filter_var($data['contractor_no_from'], FILTER_SANITIZE_STRING);
        $sale_data['contractor_no_to'] = filter_var($data['contractor_no_to'], FILTER_SANITIZE_STRING);
        $sale_data['ref'] = filter_var($data['ref'], FILTER_SANITIZE_STRING);
        $sale_data['amount'] = filter_var($data['amount'], FILTER_SANITIZE_STRING);
        $sale_data['coins'] = filter_var($data['coins'], FILTER_SANITIZE_STRING);
        require 'config/dbconnect.php';

        $db->where ("contractor_no_from", $sale_data['contractor_no_from']);
        $db->where ("contractor_no_to", $sale_data['contractor_no_to']);
        $db->where ("ref", $sale_data['ref']);
        $db->where ("amount", $sale_data['amount']);
        $db->where ("coins", $sale_data['coins']);
        $db->getOne ("sell_coins");

        if($db->count>0){
            $response = array(
                'code'=>329,
                'message'=>'Transaction already created'
            );
        }else{

            $db->insert("sell_coins",$data);
            if ($db->count>0){

                $db->where ("contractor_no", $sale_data['contractor_no_from']);
                $user = $db->getOne ("tbl_users");
                if($db->count>0){
                    $emailFormat = 'invoiceSeller';
                    include 'emails/emails.php';
                    if(!$mail->send()) {
                        $response = array(
                            'code'=>328,
                            'message'=>'Mail error',
                            'mail'=>$mail->ErrorInfo
                        );
                    }else{
                    $response = array(
                        'code'=>328,
                        'message'=>'Transaction created'
                    );
                    }
                }

            }else{
                $response = array(
                    'code'=>329,
                    'message'=>'Transaction creation failed'
                );
            }

        }
        return json_encode($response);
    }
    public function getSalesFrom($data){

        require 'config/dbconnect.php';

        //Pending Sales//
        $db->where ("contractor_no_from",  $data['contractor_no_from']);
        $db->where ("status",  'Pending');
        $pending_sales = $db->get ("sell_coins");

        if($db->count>0){
            $pending_sales = $pending_sales;
        }else{
            $pending_sales = 'No Pending Transactions';
        }
        //Processed Sales//
        $db->where ("contractor_no_from",  $data['contractor_no_from']);
        $db->where ("status",  'Processed');
        $processed_sales = $db->get ("sell_coins");

        if($db->count>0){
            $processed_sales = $processed_sales;
        }else{
            $processed_sales = 'No Processed Transactions';
        }
        $response = array(
            'code'=>'936',
            'pending'=>$pending_sales,
            'processed'=>$processed_sales,
        );
        return json_encode($response,JSON_PRETTY_PRINT);

    }
    public function transactions($label){

        require 'config/dbconnect.php';
        $pendingTransactions = [];
        $processedTransactions = [];
        //Pending Sales//
        $db->where ("contractor_no_from",  $label);
        $db->orWhere ("contractor_no_to",  $label);
        $pending_sales = $db->get ("sell_coins");

        if($db->count>0){
            foreach ($pending_sales as $transactions){

                if($transactions['status']=='Pending'){
                    $pendingTransactions[] = [
                        'ref'=>$transactions['ref'],
                        'address_from'=>$transactions['address_from'],
                        'address_to'=>$transactions['address_to'],
                        'rate'=>$transactions['rate'],
                        'amount'=>$transactions['amount'],
                        'coins'=>$transactions['coins'],
                        'status'=>$transactions['status'],
                    ];
                }elseif($transactions['status']=='Processed'){

                    $processedTransactions[] = [
                        'ref'=>$transactions['ref'],
                        'address_from'=>$transactions['address_from'],
                        'address_to'=>$transactions['address_to'],
                        'rate'=>$transactions['rate'],
                        'amount'=>$transactions['amount'],
                        'coins'=>$transactions['coins'],
                        'status'=>$transactions['status'],
                    ];
                }
            }

            $response = [

                'code'=>'2012',
                'processed'=>$processedTransactions,
                'pending'=>$pendingTransactions,
            ];
        }
        else{
            $response = [

                'code'=>'2012',
                'message'=>'No transactions'
            ];
        }

        return json_encode($response, JSON_PRETTY_PRINT);
    }

}