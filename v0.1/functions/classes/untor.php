<?php
/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/07/11
 * Time: 3:08 PM
 */

class untor {

    /****** CONFIG *******/
    private $apache_version = 2.4; //Put here your Apache version, just the major and subversion (eg. for 1.3.5 set to 1.3, for 2.4.10 set to 2.4)
    private $refresh_interval = 60; //Interval (in minutes) that the cache must be refreshed
    private $cache_folder = "cache/"; /*Relative or absolute path to where the system should keep a cache file with exit nodes.
    Make sure the folder is writable.*/
    /****** CONFIG END - Nothing to configure beyond this line ******/

    private $cache_fld;
    private $nodes_url = "https://check.torproject.org/exit-addresses";
    private $raw_node_list;
    private $node_list;

    /****************************
     * Constructor of the class
     *****************************/
    public function __construct(){
        //Check whether the cache folder is a relative or absolute path
        if(substr($this->cache_folder,0,1) == "/"){
            $this->cache_fld = $this->cache_folder;
        }else{
            $curdir = dirname(__FILE__);
            $this->cache_fld = $curdir . "/" . $this->cache_folder;
        }
        //Check if cache folder is writable
        if(!is_writable($this->cache_fld)) die("CACHE FOLDER IS NOT WRITEABLE!");
        //Check for the protection .htaccess file
        if(!file_exists($this->cache_fld . "/.htaccess")){
            if($this->apache_version >= 2.4){
                $content = "Require all denied\r\n";
            }else{
                $content = "Order deny,allow\r\nDeny from all\r\n";
            }
            file_put_contents($this->cache_fld . "/.htaccess",$content);
        }
        //Checks if there's a cached file
        if(file_exists($this->cache_fld . "/cached.untor")){
            $now = time();
            $update = filemtime($this->cache_fld . "/cached.untor");
            if($update + ($this->refresh_interval * 60) > $now){
                $this->getList();
            }
        }else{
            $this->getList();
        }
        $tors = file_get_contents($this->cache_fld . "/cached.untor");
        if(strstr($tors,$_SERVER['REMOTE_ADDR'])){
            include($curdir . "/untor.html");
            die();
        }
    }

    /****************************
     * Gets the current node list
     *****************************/
    private function getList(){
        $now = time();
        //Check the update lock
        if(file_exists($this->cache_fld . "/update-lock")){
            $ctime = filemtime($this->cache_fld . "/update-lock");
            if($ctime + 600 < $now){
                //Last update is running for more than 10 minutes, probably hang, let's kill it
                unlink($this->cache_fld . "/update-lock");
            }else{
                return; //An update is still running, use the old file meanwhile
            }
        }
        file_put_contents($this->cache_fld . "/update-lock","1"); //Create an update lock file
        //Get the current node list
        $this->raw_node_list = @file_get_contents($this->nodes_url);
        if(!$this->raw_node_list) {
            unlink($this->cache_fld . "/update-lock");
            return; //Couldn't get the node list, so we are resumed to the cached version
        }
        $this->processList();
        if(!empty($this->node_list)){
            $this->createCache();
        }
        unlink($this->cache_fld . "/update-lock");
        return;
    }

    /****************************
     * Process the raw node list
     *****************************/
    private function processList(){
        $x = explode("\n",$this->raw_node_list);
        foreach($x as $v){
            if(strstr($v,"ExitAddress")){
                $line = explode(" ",trim($v));
                if(filter_var($line[1], FILTER_VALIDATE_IP)){
                    $this->node_list[] = $line[1];
                }
            }
        }
    }
    /****************************
     * Creates the cache file
     *****************************/
    private function createCache(){
        $out = implode("|",$this->node_list);
        file_put_contents($this->cache_fld . "/cached.untor",$out);
    }

}