<?php

/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/06/21
 * Time: 11:25 AM
 */

class cards{


    private $id = '';
    public function getCard($id){
        $deploy='';
        require 'config/dbconnect.php';
        require 'config/keygen.php';
        require 'config/config.php';

        $this->id = $id;
        $db->where ("contractor_no", $this->id);
        $cardInfo =  $db->getOne("allocatecard");

        if($db->count>0){

            $cardNumber = $cardInfo['cardNumber'];
            if($cardNumber!=null){

                $webserviceCONFIG = new WebServiceConfiguration();
                $setTerminalID = $webserviceCONFIG->setTerminalID();
                $setWebURL = $webserviceCONFIG->setWebServiceURL();
                $setTerminalPassword = $webserviceCONFIG->setTerminalPassword();
                $transactionID = $OTP;
                $currentDateAndTime = new TimeAndDateINiso8601();
                $transactionDate = $currentDateAndTime->getCurrentTime();
                $methodName = 'Balance';
                $stringTobeHashed = $methodName.$setTerminalID.$profileNumber.$cardNumber.$transactionID.$transactionDate;
                $checkSum = hash_hmac('sha1',$stringTobeHashed, $setTerminalPassword);

                $request = xmlrpc_encode_request($methodName,

                    array(
                        $setTerminalID,
                        $profileNumber,
                        $cardNumber,
                        $transactionID,
                        date("Y-m-d H:i:s"),
                        $checkSum,
                    )
                );

//create the stream context for the request
                $context = stream_context_create(array('http' => array(
                    'method' => "POST",
                    'header' => "Content-Type: text/xml\r\nUser-Agent: PHPRPC/1.0\r\n",
                    'content' => $request
                )));

//URL of the XMLRPC Server
                $server = $setWebURL;
                $file = file_get_contents($server, false, $context);
//decode the XMLRPC response
                $response = xmlrpc_decode($file);

            }

        }
        else{
            $response = array(
                'code'=>'414',
                'message'=>'No card allocated to the profile please link card the GBC card to the profile'
            );
        }
        return json_encode($response, JSON_PRETTY_PRINT);


    }
    public function allocateCard($data){


        require 'config/dbconnect.php';
        require 'config/keygen.php';
        require 'config/config.php';
        require 'card_functions/linkCard.php';
        require 'card_functions/allocatecard.php';

        return json_encode($response);

    }
    public function loadCard($data){


        require 'config/dbconnect.php';
        require 'config/keygen.php';
        require 'config/config.php';
        require 'card_functions/loadcard.php';

        return json_encode($response);

    }
    public function statement($contractor_no){
        require 'config/dbconnect.php';
        require 'config/keygen.php';
        require 'config/config.php';
        require 'card_functions/statement.php';

        return json_encode($responseStatement, JSON_PRETTY_PRINT);

    }
    public function reverseFundsToprofile($data){


        require 'config/dbconnect.php';
        require 'config/keygen.php';
        require 'config/config.php';
        require 'card_functions/deductcard.php';

        return json_encode($response);

    }
    public function fees($data){


        require 'config/dbconnect.php';
        require 'config/keygen.php';
        require 'config/config.php';
        require 'card_functions/fees.php';

        return json_encode($response);

    }
}