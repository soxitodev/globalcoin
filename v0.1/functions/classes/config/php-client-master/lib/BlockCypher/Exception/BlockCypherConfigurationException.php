<?php

namespace BlockCypher\Exception;

/**
 * Class BlockCypherConfigurationException
 *
 * @package BlockCypher\Exception
 */
class BlockCypherConfigurationException extends \Exception
{

    /**
     * Default Constructor
     *
     * @param string|null $response
     * @param int $code
     */
    public function __construct($response = null, $code = 0)
    {
        parent::__construct($response, $code);
    }
}