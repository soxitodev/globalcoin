<?php

namespace BlockCypher\Exception;

/**
 * Class BlockCypherInvalidCredentialException
 *
 * @package BlockCypher\Exception
 */
class BlockCypherInvalidCredentialException extends \Exception
{

    /**
     * Default Constructor
     *
     * @param string|null $response
     * @param int $code
     */
    public function __construct($response = null, $code = 0)
    {
        parent::__construct($response, $code);
    }

    /**
     * prints error message
     *
     * @return string
     */
    public function errorMessage()
    {
        $errorMsg = 'Error on line ' . $this->getLine() . ' in ' . $this->getFile()
            . ': <b>' . $this->getMessage() . '</b>';
        return $errorMsg;
    }

}
