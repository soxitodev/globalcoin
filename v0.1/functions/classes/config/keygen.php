<?php
/**
 * Created by PhpStorm.
 * User: Sakhile
 * Date: 2016/05/18
 * Time: 11:23 AM
 */


/**
 * ------------
 * BEGIN CONFIG
 * ------------
 * Edit the configuraion
 */
function base321_decode($b32) {
    $lut = array("A" => 0,       "B" => 1,
        "C" => 2,       "D" => 3,
        "E" => 4,       "F" => 5,
        "G" => 6,       "H" => 7,
        "I" => 8,       "J" => 9,
        "K" => 10,      "L" => 11,
        "M" => 12,      "N" => 13,
        "O" => 14,      "P" => 15,
        "Q" => 16,      "R" => 17,
        "S" => 18,      "T" => 19,
        "U" => 20,      "V" => 21,
        "W" => 22,      "X" => 23,
        "Y" => 24,      "Z" => 25,
        "2" => 26,      "3" => 27,
        "4" => 28,      "5" => 29,
        "6" => 30,      "7" => 31
    );

    $b32    = strtoupper($b32);
    $l      = strlen($b32);
    $n      = 0;
    $j      = 0;
    $binary = "";

    for ($i = 0; $i < $l; $i++) {

        $n = $n << 5;
        $n = $n + $lut[$b32[$i]];
        $j = $j + 5;

        if ($j >= 8) {
            $j = $j - 8;
            $binary .= chr(($n & (0xFF << $j)) >> $j);
        }
    }

    return $binary;
}

function get_timestamp2() {
    return floor(microtime(true)/30);
}


function binary_key2($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$trans = binary_key2();

$binary_key = $trans;

$todayDate = date("i:s");

$binary_timestamp = pack('N*', 0) . pack('N*', $todayDate);

$hash = hash_hmac('sha1', $binary_timestamp, $binary_key, true);

$offset = ord($hash[19]) & 0xf;

$OTP = (
        ((ord($hash[$offset+0]) & 0x7f) << 24 ) |
        ((ord($hash[$offset+1]) & 0xff) << 16 ) |
        ((ord($hash[$offset+2]) & 0xff) << 8 ) |
        (ord($hash[$offset+3]) & 0xff)
    ) % pow(10, 6);


function getSalt() {
    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
    $randStringLen = 32;

    $randString = "";
    for ($i = 0; $i < $randStringLen; $i++) {
        $randString .= $charset[mt_rand(0, strlen($charset) - 1)];
    }

    return $randString;
}

$salt = getSalt();
$salt = base64_encode($salt);
$salt = str_replace('+', '.', $salt);

$transact =  $OTP *3*12;

$mobileCode = $OTP *2;
function keygen($length=10)
{
    $key = '';
    list($usec, $sec) = explode(' ', microtime());
    mt_srand((float) $sec + ((float) $usec * 100000));

    $inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));

    for($i=0; $i<$length; $i++)
    {
        $key .= $inputs{mt_rand(0,61)};
    }
    return $key;
}

function generateStrongPassword($length = 5, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    //if(strpos($available_sets, 'l') !== false)
       // $sets[] = 'abcdefghjkmnpqrstuvwxyz';
   // if(strpos($available_sets, 'u') !== false)
      //  $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if(strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
   // if(strpos($available_sets, 's') !== false)
       // $sets[] = '!@#$%&*?';
    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if(!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function generateMerchantKey($length = 34, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if(strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if(strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if(strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if(!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}


//Generates Collection Code//

function serverTransactionKey($length = 7, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if(strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if(!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}
function masterCardTransKey($length = 8, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if(strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if(!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}
function consumer_no($length = 7, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if(strpos($available_sets, 'd') !== false)
        $sets[] = '1234567890';
    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if(!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

$transactionKey = serverTransactionKey();
$masterCardTransKey = masterCardTransKey();

$consumer_no = consumer_no(6);