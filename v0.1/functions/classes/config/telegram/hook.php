<?php
/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/07/26
 * Time: 1:40 PM
 */

try {
    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($API_KEY, $BOT_NAME);

    // Handle telegram webhook request
    $telegram->handle();
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    // Silence is golden!
    // log telegram errors
    // echo $e;
}