<?php
//require_once '../../../../vendor/autoload.php';

use MasterCard\Core\ApiConfig;
use MasterCard\Core\Security\OAuth\OAuthAuthentication;

//require '../config/keygen.php';
$consumerKey = "jLBNv1XykymY2TLGVNQxCAvEAY07ffvpbbcrHwBu4fbed450!af6c1db3f29a4cb8ac0a68116418f37a0000000000000000";   // You should copy this from "My Keys" on your project page e.g. UTfbhDCSeNYvJpLL5l028sWL9it739PYh6LU5lZja15xcRpY!fd209e6c579dc9d7be52da93d35ae6b6c167c174690b72fa
$keyAlias = "keyalias";   // For production: change this to the key alias you chose when you created your production key
$keyPassword = "keystorepassword";   // For production: change this to the key alias you chose when you created your production key
$privateKey = file_get_contents(getcwd()."/functions/classes/master_card/certificates/global-coin-payments-1498575828-sandbox.p12"); // e.g. /Users/yourname/project/sandbox.p12 | C:\Users\yourname\project\sandbox.p12
ApiConfig::setAuthentication(new OAuthAuthentication($consumerKey, $privateKey, $keyAlias, $keyPassword));
ApiConfig::setDebug(true);
ApiConfig::setSandbox(true);   // For production: use ApiConfig::setSandbox(false)

function getDatetimeNow() {
    $tz_object = new DateTimeZone('Africa/Johannesburg');
    //date_default_timezone_set('Brazil/East');

    $datetime = new DateTime();
    $datetime->setTimezone($tz_object);
    $month = $datetime->format('m');
    $year = $datetime->format('y');

    $date = $month.$year;
    return $date;
}