<?php
/**
 * Created by PhpStorm.
 * User: Layby Cafe
 * Date: 2017/06/28
 * Time: 3:05 PM
 */
namespace MasterCard\Api\Moneysend;
use MasterCard\Core\Model\RequestMap;

$map = new RequestMap();
$map->set("PaymentRequestV3.LocalDate", getDatetimeNow());
$map->set("PaymentRequestV3.LocalTime", "150149");
$map->set("PaymentRequestV3.TransactionReference", $TransactionReference);
$map->set("PaymentRequestV3.SenderName.First", $SenderiNFO['first_name']);
$map->set("PaymentRequestV3.SenderName.Middle", @$SenderiNFO['middle_name']);
$map->set("PaymentRequestV3.SenderName.Last", $SenderiNFO['last_name']);
$map->set("PaymentRequestV3.SenderPhone", $SenderiNFO['phone']);
$map->set("PaymentRequestV3.SenderDateOfBirth", $SenderiNFO['d_o_b']);
$map->set("PaymentRequestV3.SenderAddress.Line1", $SenderAddress['address1']);
$map->set("PaymentRequestV3.SenderAddress.Line2", $SenderAddress['address2']);
$map->set("PaymentRequestV3.SenderAddress.City", $SenderAddress['city']);
$map->set("PaymentRequestV3.SenderAddress.CountrySubdivision", $SenderAddress['countrySubdivision']);
$map->set("PaymentRequestV3.SenderAddress.PostalCode", $SenderAddress['postalCode']);
$map->set("PaymentRequestV3.SenderAddress.Country", $SenderAddress['country']);
$map->set("PaymentRequestV3.FundingCard.AccountNumber", "5184680470000023");
$map->set("PaymentRequestV3.FundingSource", "03");
$map->set("PaymentRequestV3.AdditionalMessage", "Test");
$map->set("PaymentRequestV3.ParticipationId", "Test");
$map->set("PaymentRequestV3.LanguageIdentification", "Tes");
$map->set("PaymentRequestV3.LanguageData", "Test");

//Receiver DATA//
$map->set("PaymentRequestV3.ReceivingCard.AccountNumber", "5184680430000006");
$map->set("PaymentRequestV3.ReceiverName.Middle", "B");
$map->set("PaymentRequestV3.ReceiverName.Last", "Lopez");
$map->set("PaymentRequestV3.ReceiverAddress.Line1", "Pueblo Street");
$map->set("PaymentRequestV3.ReceiverAddress.Line2", "PO BOX 12");
$map->set("PaymentRequestV3.ReceiverAddress.City", "El PASO");
$map->set("PaymentRequestV3.ReceiverAddress.CountrySubdivision", "TX");
$map->set("PaymentRequestV3.ReceiverAddress.PostalCode", "79906");
$map->set("PaymentRequestV3.ReceiverAddress.Country", "USA");
$map->set("PaymentRequestV3.ReceiverPhone", "1800639426");
$map->set("PaymentRequestV3.ReceiverDateOfBirth", "06211977");
$map->set("PaymentRequestV3.ReceivingAmount.Value", "737");
$map->set("PaymentRequestV3.ReceivingAmount.Currency", "840");

//Bank information//

$map->set("PaymentRequestV3.ICA", "009674");
$map->set("PaymentRequestV3.ProcessorId", "9000000442");
$map->set("PaymentRequestV3.RoutingAndTransitNumber", "990442082");
$map->set("PaymentRequestV3.CardAcceptor.Name", "THE BEST BANK");
$map->set("PaymentRequestV3.CardAcceptor.City", "ANYTOWN");
$map->set("PaymentRequestV3.CardAcceptor.State", "MO");
$map->set("PaymentRequestV3.CardAcceptor.PostalCode", "99999-1234");
$map->set("PaymentRequestV3.CardAcceptor.Country", "USA");
$map->set("PaymentRequestV3.TransactionDesc", "P2P");
$map->set("PaymentRequestV3.MerchantId", "123456");

//Identification Receiver//

$map->set("PaymentRequestV3.ReceiverIdentification.Type", "01");
$map->set("PaymentRequestV3.ReceiverIdentification.Number", "2147483647");
$map->set("PaymentRequestV3.ReceiverIdentification.CountryCode", "USA");
$map->set("PaymentRequestV3.ReceiverIdentification.ExpirationDate", "10102017");
$map->set("PaymentRequestV3.ReceiverNationality", "USA");
$map->set("PaymentRequestV3.ReceiverCountryOfBirth", "USA");

//Identification Sender//
$map->set("PaymentRequestV3.SenderIdentification.Type", "01");
$map->set("PaymentRequestV3.SenderIdentification.Number", "2147483647");
$map->set("PaymentRequestV3.SenderIdentification.CountryCode", $SenderAddress['country']);
$map->set("PaymentRequestV3.SenderIdentification.ExpirationDate", "10102017");
$map->set("PaymentRequestV3.SenderNationality", $SenderAddress['country']);
$map->set("PaymentRequestV3.SenderCountryOfBirth", $SenderAddress['country']);
$map->set("PaymentRequestV3.TransactionPurpose", "01");

$response = Payment::create($map);

$paymentData = array(
    'code'=>$response->get("Transfer.TransactionHistory.Transaction.Response.Code"),
    'date'=>getDatetimeNow(),
    'RequestId'=>$response->get("Transfer.RequestId"),
    'TransactionReference'=>$response->get("Transfer.TransactionReference"),
    'TransactionType'=>$response->get("Transfer.TransactionHistory.Transaction.Type"),
    'SystemTraceAuditNumber'=>$response->get("Transfer.TransactionHistory.Transaction.SystemTraceAuditNumber"),
    'NetworkReferenceNumber'=>$response->get("Transfer.TransactionHistory.Transaction.NetworkReferenceNumber"),
    'SettlementDate'=>$response->get("Transfer.TransactionHistory.Transaction.SettlementDate"),
    'Description'=>$response->get("Transfer.TransactionHistory.Transaction.Response.Description"),
    'SubmitDateTime'=>$response->get("Transfer.TransactionHistory.Transaction.SubmitDateTime"),

);