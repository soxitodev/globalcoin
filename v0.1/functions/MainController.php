<?php
//namespace laybyApi;
date_default_timezone_set("Africa/Johannesburg");

include 'classes/users.php';
include 'classes/bitcoins.php';
include 'classes/cards.php';
include 'classes/password.php';
include 'classes/masterCard.php';
include 'classes/visa.php';
include 'classes/fica.php';
include 'classes/ethereum.php';
include 'classes/bloccypher.php';